package tjvm.loader;

import java.util.Vector;

/**
 * Class that holds all information about the fields (&sect;4.5) of the {@code class} file after loading.
 * The fields are saved as {@link Field}s.
 */
public class FieldList {

    /**
     * Collection that holds all fields.
     */
    private Vector<Field> fields;

    /**
     * Create an empty list of fields.
     */
    FieldList() {
        fields = new Vector<>();
    }

    /**
     * Get the number of fields in the list.
     *
     * @return the number of fields
     */
    public int size() {
        return fields.size();
    }

    /**
     * Add a field to the field list.
     *
     * @param f the field to add
     * @throws InternalError if field is {@code null}
     */
    void addField(Field f) {
        if (f != null) {
            fields.add(f);
        } else {
            throw new InternalError("Error at FieldList.addField(): Given Field reference was null.");
        }
    }

    /**
     * Get a field from the field list.
     *
     * @param index index of the entry to return
     * @return the entry at the specified index
     */
    public Field getEntry(int index) {
        return fields.get(index);
    }

    /**
     * Check if a field is contained in the field list.
     *
     * @param identifier the identifier of the field
     * @return {@code true} if field is in field list
     */
    boolean containsIdentifier(String identifier) {
        for (Field field: fields)
            if (field.identifier.equals(identifier))
                return true;
        return false;
    }

    /**
     * Returns the string representation of the fieldList where every field is indented.
     * The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of all fieldList
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (Field field : fields)
            res.append(field.toString()).append("\n\n");
        return res.toString();
    }
}