package tjvm.loader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class loader is able to transfer {@code class} files from the file system of the computer into a
 * {@link DerivedClass}, which is easier to handle than the byte-oriented representation. <p>
 * The way this class loader operates, is adapted from "The Java Virtual Machine Specification" (Java SE 10 Edition).
 * All references to the chapters are written with a section symbol e.g. &sect;2.5<p>
 * While the {@code class} file format is defined all through &sect;4.1 to &sect;4.8, the process of loading is described in &sect;5.3.
 * For TinyJava a lot of the mentioned specifications don't apply or apply a little differently. They are adapted for the
 * lesser language size without mentioning every single change. <p>
 * In order to differentiate more between LOADING and CREATION (both &sect;5.3) the creation is done by
 * {@link tjvm.data.MethodArea#create(DerivedClass)} while only the loading is done by the class loader.
 */
public class ClassLoader {

    /**
     * The path the class loader is looking for files to load.
     */
    private String pathName;
    /**
     * Collection to keep track of already loaded classes.
     */
    private HashMap<String, DerivedClass> loadedClasses;


    /**
     * Create a new <code>ClassLoader</code> that is able to find and load classes from a given
     * directory in the file system.
     *
     * @param pathName The path of the folder containing the classes for this <code>ClassLoader</code> instance
     */
    public ClassLoader(String pathName) {
        this.pathName = pathName;
        loadedClasses = new HashMap<>();
    }

    /**
     * Loading a {@code class} file from the file system, which consists of following steps from the JVM Specification
     * in &sect;5.3.1 (Loading Using the Bootstrap Class Loader) and &sect;5.3.5 (Deriving a Class from a {@code class} file).
     * <ol>
     * <li>Check, if the class has been loaded already. If so, no class loading is necessary. But this should have
     * been checked by the virtual machine already, so a {@code LinkageError is thrown}</li>
     * <li>Find the file in the file system. If If no purported representation of the is found,
     * loading throws an instance of {@code ClassNotFoundException}.</li>
     * <li>Derive the class from the file (attempting to parse the purported representation) while detecting
     * following errors:
     * <ul>
     * <li>The {@code class} file has a valid {@code ClassFile} structure (&sect;4.1, &sect;4.8), otherwise a
     * {@code ClassFormatError} is thrown.</li>
     * <li>The {@code class} file actually represents a class with that name, otherwise a
     * {@code NoClassDefFoundError} is thrown.</li>
     * </ul>
     * </li>
     * <li>The class loader records, that is the initiating loader of that class.</li>
     * </ol>
     *
     * @param className the name of the {@code class} file
     * @return the derived class
     * @throws LinkageError           if class has been already loaded
     * @throws ClassNotFoundException if no purported representation for the className is found
     * @throws InternalError          if the stream can't be initialized
     */
    public DerivedClass load(String className) throws ClassNotFoundException {
        String fileName = className + ".class";
        if (loadedClasses.containsKey(className))
            throw new LinkageError("Class with the name " + className + " already loaded.");
        try {
            DataInputStream inStream = new DataInputStream(new FileInputStream(Paths.get(pathName, fileName).toFile()));
            DerivedClass der = new DerivedClass(className);
            deriveClassFile(inStream, der);
            loadedClasses.put(className, der);
            return der;
        } catch (FileNotFoundException e) {
            throw new ClassNotFoundException("File \"" + fileName + "\" not found.");
        } catch (NullPointerException e) {
            throw new InternalError("No file stream has been initialized.");
        }
    }

    /**
     * Tries to derive a class from a {@code DataInputStream}. Deriving a class includes
     * Format Checking (&sect;4.8) of the {@code class} file and the parsing into an internal structure
     * of the virtual machine. The methods calls other methods according to the {@code ClassFile}
     * Structure (&sect;4.1) in order to make the derivation more transparent.
     *
     * @param in  the stream of the {@code class} file to derive
     * @param der the representation of the class to write in
     */
    private void deriveClassFile(DataInputStream in, DerivedClass der) {
        checkMagicNumber(in);
        deriveVersionNumber(in, der);
        deriveConstantPool(in, der);
        deriveAccessFlags(in, der);
        deriveThisSuper(in, der);
        deriveInterfaces(in, der);
        deriveFields(in, der);
        deriveMethods(in, der);
        deriveAttributes(in, der, null, null);
        checkEndOfFile(in);
    }

    /**
     * Each java class-file begins with 4-Bytes that have the value <code>OxCAFEBABE</code>. (&sect;4.1)
     * If they're different, the class is not valid.
     *
     * @param in the current input stream
     * @throws ClassFormatError When magic number is wrong or EOF reached (or other IO exception)
     */
    private void checkMagicNumber(DataInputStream in) {
        try {
            int magicNumber = in.readInt();
            if (magicNumber != 0xCAFEBABE) {
                throw new ClassFormatError("Wrong magic number.");
            }
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading magic number (or other IO exception).");
        }
    }

    /**
     * Derive the major and minor class file version from the {@code class} file.
     * As there is no different behaviour planned for different versions
     * of TinyJava, it is just read and written into the derived class. Under normal circumstances
     * the machine must check, if the version is supported. (&sect;4.1)
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError When EOF is reached (or other IO exception)
     */
    private void deriveVersionNumber(DataInputStream in, DerivedClass der) {
        try {
            der.minorVersion = Short.toUnsignedInt(in.readShort());
            der.majorVersion = Short.toUnsignedInt(in.readShort());
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading version number (or other IO exception).");
        }
    }

    /**
     * Derive the constant pool from the {@code class} file. There are various constraints
     * for the constant pool, which is a table of variable-sized structures (&sect;4.4). Right
     * before the content of the table is the the length written down within 2 bytes (&sect;4.1).
     * <p>
     * At the end of derivation, the Constant Pool undergoes further Format Checking.
     * This is necessary in order to ensure that the constraints in the JVM Specification are met.
     * (for example: Class entries always have to point to a valid UTF8 entry)
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError If constant pool tag not included in TinyJava or EOF is reached (or other IO exception)
     */
    private void deriveConstantPool(DataInputStream in, DerivedClass der) {
        try {
            int cpSize = Short.toUnsignedInt(in.readShort());

            for (int i = 1; i < cpSize; i++) {
                CPEntry cpEntry;
                byte tag = in.readByte(); //get tag of constant pool entry

                switch (tag) {
                    case CPEntry.CONSTANT_Utf8: //character string
                        int length = Short.toUnsignedInt(in.readShort());
                        byte[] utf8Bytes = new byte[length];
                        in.readNBytes(utf8Bytes, 0, length);
                        cpEntry = new CPUTF8Entry(new String(utf8Bytes, StandardCharsets.UTF_8));
                        break;

                    case CPEntry.CONSTANT_Integer: //integer constant
                        int constVal = in.readInt();
                        cpEntry = new CPIntEntry(constVal);
                        break;

                    case CPEntry.CONSTANT_Class: //class reference, class name: at index in following two bytes
                        int classNameIndex = Short.toUnsignedInt(in.readShort());
                        cpEntry = new CPClassEntry(classNameIndex);
                        break;

                    case CPEntry.CONSTANT_Fieldref: //field reference, with class, name and descriptor (int, int[] or a reference to a TinyJava class)
                    case CPEntry.CONSTANT_Methodref: //method reference, with class, name and descriptor (int, int[] or a reference to a TinyJava class)
                        int classIndex = Short.toUnsignedInt(in.readShort());
                        int nameAndTypeIndex = Short.toUnsignedInt(in.readShort());
                        cpEntry = new CPFieldOrMethEntry(classIndex, nameAndTypeIndex);
                        break;

                    case CPEntry.CONSTANT_NameAndType:
                        int nameIndex = Short.toUnsignedInt(in.readShort());
                        int descriptorIndex = Short.toUnsignedInt(in.readShort());
                        cpEntry = new CPNameAndTypeEntry(nameIndex, descriptorIndex);
                        break;

                    default:
                        throw new ClassFormatError("Error at deriving the constant pool - tag " + tag + " not included in TinyJava.");
                }

                cpEntry.cpIndex = i;
                cpEntry.tag = tag;
                der.constantPool.addEntry(cpEntry);
            }

        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading constant pool (or other IO exception).");
        }
        der.constantPool.checkFormat();
    }

    /**
     * Derive the access flags from the class file (&sect;4.1). A TinyJava file should only have the <code>ACC_SUPER</code> flag (<code>0x0020</code>).
     * The other flags, for example <code>ACC_PUBLIC</code> or <code>ACC_ENUM</code> are not valid in TinyJava.
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError When wrong access flags are set or EOF is reached (or other IO exception)
     */
    private void deriveAccessFlags(DataInputStream in, DerivedClass der) {
        try {
            short flags = in.readShort();
            if ((flags & 0x0020) == 0x0020 && (flags & 0xF611) == 0)
                der.accessFlags = flags;
            else
                throw new ClassFormatError("Access flags are not valid.");
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading access flags (or other IO exception).");
        }
    }

    /**
     * Derive the <code>this_class</code> and <code>super_class</code> items from the class file.
     * They must both have a valid index into the constant pool table to a class info structure (&sect;4.1).
     * The super class of every class in TinyJava can only be <code>java/lang/Object</code>.
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError     When wrong access flags are set or EOF is reached (or other IO exception)
     * @throws NoClassDefFoundError if the purported representation does not actually represent a class with that name
     */
    private void deriveThisSuper(DataInputStream in, DerivedClass der) {
        try {
            //this_class
            der.thisIndex = Short.toUnsignedInt(in.readShort());
            if (der.constantPool.entryIsType(der.thisIndex, CPEntry.CONSTANT_Class)) {
                int ind = der.constantPool.getEntryAs(der.thisIndex, CPClassEntry.class).classNameIndex;
                String classEntry = der.constantPool.getEntryAs(ind, CPUTF8Entry.class).utf8String;
                if (!(classEntry).equals(der.className)) {
                    throw new NoClassDefFoundError("Class name and name of loaded class file are not equal.");
                }
            } else {
                throw new ClassFormatError("Index of this_class doesn't point to class info structure");
            }

            //super_class
            der.superIndex = Short.toUnsignedInt(in.readShort());
            if (der.constantPool.entryIsType(der.superIndex, CPEntry.CONSTANT_Class)) {
                int ind = der.constantPool.getEntryAs(der.superIndex, CPClassEntry.class).classNameIndex;
                if (!der.constantPool.getEntryAs(ind, CPUTF8Entry.class).utf8String.equals("java/lang/Object")) {
                    throw new ClassFormatError("Super class ist not java/lang/Object");
                }
            } else {
                throw new ClassFormatError("Index of this_class or super_class doesn't point to class info structure");
            }

        } catch (
                IOException e) {
            throw new ClassFormatError("EOF while reading this_class and super_class indices (or other IO exception).");
        }
    }

    /**
     * Derives the <code>interface_count</code> fromm the class file (&sect;4.1). The <code>interface_count</code> should be 0 and
     * therefore the array <code>interfaces[]</code> is empty. This is, because there are no interfaces in TinyJava.
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError When wrong access flags are set or EOF is reached (or other IO exception)
     */
    private void deriveInterfaces(DataInputStream in, DerivedClass der) {
        try {
            der.interfaceCount = Short.toUnsignedInt(in.readShort());
            if (der.interfaceCount != 0) {
                throw new ClassFormatError("Interface count is not zero.");
            }
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading interface_count (or other IO exception).");
        }
    }

    /**
     * Derive the <code>field_count</code> and the table of <code>field_info</code> structures from the class file (&sect;4.1).
     * While deriving the fieldList, the following checks are made (&sect;4.5):
     * <ul>
     * <li>Are the fieldList final and/or static? Any other access flags are not valid.</li>
     * <li>Are the <code>name_index</code> and the <code>descriptor_index</code> pointing to a valid
     * entry in the constant pool table?</li>
     * <li>Are the UTF8 values <code>name_index</code> and the <code>descriptor_index</code> well-formed?</li>
     * <li>Each value of the {@code attributes} table must be a {@code attribute_info} structure (which is checked
     * in the method {@code deriveAttributes()}).</li>
     * <li>There can be no final field other than basic int types.</li>
     * <li>There can be no two field with the same name and descriptor.</li>
     * </ul>
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError if fieldList are malformed, not valid or EOF is reached (or other IO exception)
     */
    private void deriveFields(DataInputStream in, DerivedClass der) {
        try {
            int fieldsCount = Short.toUnsignedInt(in.readShort());
            for (int i = 0; i < fieldsCount; i++) {

                Field field = new Field();

                //read access flags
                short accFlags = in.readShort();
                if ((accFlags & 0x5C07) != 0)
                    throw new ClassFormatError("Access flags are not valid.");
                field.isStatic = (accFlags & 0x0008) == 0x0008;
                field.isFinal = (accFlags & 0x0010) == 0x0010;
                field.flags = accFlags;

                //derive field name
                int fieldNameIndex = Short.toUnsignedInt(in.readShort());
                if (der.constantPool.entryIsType(fieldNameIndex, CPEntry.CONSTANT_Utf8)) {
                    field.name = der.constantPool.getEntryAs(fieldNameIndex, CPUTF8Entry.class).utf8String;
                } else {
                    throw new ClassFormatError("Index to field name " + fieldNameIndex + " not valid.");
                }
                if (!TextChecker.isValidUnqualifiedFieldName(field.name))
                    throw new ClassFormatError("Field name " + field.name + " not valid.");

                //derive field descriptor
                int fieldDescriptorIndex = Short.toUnsignedInt(in.readShort());
                if (der.constantPool.entryIsType(fieldDescriptorIndex, CPEntry.CONSTANT_Utf8)) {
                    field.descriptor = der.constantPool.getEntryAs(fieldDescriptorIndex, CPUTF8Entry.class).utf8String;
                } else {
                    throw new ClassFormatError("Index to field " + fieldDescriptorIndex + " not valid.");
                }

                if (!TextChecker.isValidFieldDescriptor(field.descriptor))
                    throw new ClassFormatError("Field descriptor " + field.descriptor + " not valid.");

                switch (field.descriptor) {
                    case "I":
                        field.valueType = ValueType.INT;
                        break;
                    case "[I":
                        field.valueType = ValueType.INTARR;
                        break;
                    default:
                        field.valueType = ValueType.REF;
                        break;
                }

                if (field.isFinal && field.valueType != ValueType.INT)
                    throw new ClassFormatError("Final field " + field.name + " is not of base valueType int.");

                deriveAttributes(in, der, null, field);
                field.identifier = der.className + "." + field.name + ":" + field.descriptor;
                if (der.fieldList.containsIdentifier(field.identifier))
                    throw new ClassFormatError("There can be no two fields with the same name and descriptor: " + field.identifier);
                else
                    der.fieldList.addField(field);
            }
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading fieldList (or other IO exception).");
        }
    }

    /**
     * Derive the <code>methods_count</code> and the table of <code>method_info</code> structures from the class file.
     * The return type and the type of the parameters are derived from the method descriptor.
     * While deriving the methods, the following checks are made ($4.6):
     * <ul>
     * <li>Is the method static? Any other access flags are not valid.</li>
     * <li>Are the <code>name_index</code> and the <code>descriptor_index</code> pointing to a valid
     * entry in the constant pool table?</li>
     * <li>Are the UTF8 values <code>name_index</code> and the <code>descriptor_index</code> well-formed?</li>
     * <li>Each value of the {@code attributes} table must be a {@code attribute_info} structure (which is checked
     * in the method {@code deriveAttributes()}).</li>
     * <li>There can be no <code>&lt;init&gt;</code> method with return type int or any parameters.</li>
     * <li>The method <code>&lt;clinit&gt;</code> has to have return type void
     * (and static flag set and no parameters if file is v. 51.0 or above - not checked).</li>
     * <li>There can only be 255 arguments (including {@code this}) for a method.</li>
     * </ul>
     *
     * @param in  the current input stream
     * @param der the class representation to write in
     * @throws ClassFormatError if methods are malformed, not valid or EOF is reached (or other IO exception)
     */
    private void deriveMethods(DataInputStream in, DerivedClass der) {
        try {
            int methodsCount = Short.toUnsignedInt(in.readShort());

            for (int i = 0; i < methodsCount; i++) {
                Method method = new Method();

                //read access flags
                short accFlags = in.readShort();
                if ((accFlags & 0x1DF7) != 0)
                    throw new ClassFormatError("Access flags are not valid.");
                method.isStatic = (accFlags & 0x0008) == 0x0008;
                method.flags = accFlags;

                //derive method name
                int methodNameIndex = Short.toUnsignedInt(in.readShort());
                if (der.constantPool.entryIsType(methodNameIndex, CPEntry.CONSTANT_Utf8)) {
                    method.name = der.constantPool.getEntryAs(methodNameIndex, CPUTF8Entry.class).utf8String;
                } else {
                    throw new ClassFormatError("Index to method name " + methodNameIndex + " not valid.");
                }
                if (!TextChecker.isValidUnqualifiedMethodName(method.name))
                    throw new ClassFormatError("Method name " + method.name + " not valid.");

                //derive method descriptor
                int methodDescriptorIndex = Short.toUnsignedInt(in.readShort());
                if (der.constantPool.entryIsType(methodDescriptorIndex, CPEntry.CONSTANT_Utf8)) {
                    method.descriptor = der.constantPool.getEntryAs(methodDescriptorIndex, CPUTF8Entry.class).utf8String;
                } else {
                    throw new ClassFormatError("Index to method descriptor " + methodDescriptorIndex + " not valid.");
                }
                if (!TextChecker.isValidMethDescriptor(method.descriptor, method.name))
                    throw new ClassFormatError("Method descriptor " + method.descriptor + " not valid.");


                //derive return type from method descriptor
                String ret = method.descriptor.substring(method.descriptor.indexOf(")") + 1);
                switch (ret) {
                    case "I":
                        method.returnsInt = true;
                        break;
                    case "V":
                        method.returnsInt = false;
                        break;
                    default:
                        throw new ClassFormatError("Return descriptor of method " + method.name + " " + method.descriptor + "was neither integer nor void.");
                }

                //derive parameters from method descriptor
                String parameters = method.descriptor.substring(method.descriptor.indexOf("(") + 1, method.descriptor.indexOf(")"));
                String paraPattern = TextChecker.INT_DESC + "|" + TextChecker.ARRAY_DESC + "|" + TextChecker.CLASS_DESC;
                Matcher matcher = Pattern.compile(paraPattern).matcher(parameters);
                while (matcher.find()) {
                    String res = matcher.group();
                    switch (res) {
                        case "I":
                            method.parameters.add(ValueType.INT);
                            method.parameterNames.add("int");
                            break;
                        case "[I":
                            method.parameters.add(ValueType.INTARR);
                            method.parameterNames.add("int[]");
                            break;
                        default:
                            method.parameters.add(ValueType.REF);
                            method.parameterNames.add(res.substring(1, res.length() - 1));
                            break;
                    }
                }
                method.argsSize = method.parameters.size() + (method.isStatic ? 0 : 1);
                if (method.argsSize > 255)
                    throw new ClassFormatError("Method " + method.name + " " + method.descriptor + " has too many arguments: " + method.argsSize);

                if (method.name.equals("<init>") && (method.returnsInt || method.argsSize != 1))
                    throw new ClassFormatError("Method with signature \"<init>\" not void or with parameters.");

                if (method.name.equals("<clinit>") && method.returnsInt)
                    throw new ClassFormatError("Method with signature \"<clinit>\" not void.");
                //In a class file whose version number is 51.0 or above, the class initialization method has its ACC_STATIC flag set and takes no arguments (§4.6).

                //add method, if not already there
                method.identifier = der.className + "." + method.name + ":" + method.descriptor;
                if (der.methodList.containsIdentifier(method.identifier))
                    throw new ClassFormatError("There can be no two methods with the same name and descriptor: " + method.identifier);
                else
                    der.methodList.addEntry(method);

                //derive attributes for method
                deriveAttributes(in, der, method, null);
                if (!method.hasCodeAttribute)
                    throw new ClassFormatError("Method " + method.name + " has not a Code attribute.");
            }

        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading fieldList (or other IO exception).");
        }
    }

    /**
     * Derive the <code>attribute_info</code> structures from the class file. They're use in the {@code ClassFile},
     * {@code field_info}, {@code method_info} and {@code Code_attribute} structures. <p>
     * In this version of the virtual machine, only the following structures are used ($4.7):
     * <ul>
     * <li>{@code ConstantValue}: For {@code field_info} structures. It represents the value of a constant expression.
     * If the field is static, this value is assigned to the field as part of the initialization. The constant pool
     * entry must be a {@code CONSTANT_Integer} (&sect;4.7.2).</li>
     * <li>{@code Code}: For {@code method_info} structures. It contains the virtual machine instructions and
     * auxiliary information for a method. Every {@code method_info} structure must have exactly one {@code Code}
     * attribute (&sect;4.7.3).</li>
     * <li>{@code SourceFile}: The name of the source file the {@code class} file was compiled from (&sect;4.7.10).</li>
     * <li>{@code LineNumberTable}: For {@code Code} attributes. It may be used by debuggers to determine which
     * part of the {@code code} array corresponds to a given line number in the original source file (&sect;4.7.12). </li>
     * </ul>
     *
     * @param in     the current input stream
     * @param der    the class representation to write in
     * @param method the corresponding method table entry for {@code Code} or {@code LineNumberTable} attribute the write in
     *               - may be {@code null} otherwise
     * @param field  the corresponding field for {@code ConstantValue} attribute to write in - may be {@code null} otherwise
     * @throws ClassFormatError attributes are malformed, not valid or EOF is reached (or other IO exception)
     */
    private void deriveAttributes(DataInputStream in, DerivedClass der, Method method, Field field) {
        try {
            int attCount = Short.toUnsignedInt(in.readShort());

            for (int i = 0; i < attCount; i++) {
                //check type of attribute
                int attNameIndex = Short.toUnsignedInt(in.readShort());
                String attribute;
                if (der.constantPool.entryIsType(attNameIndex, CPEntry.CONSTANT_Utf8)) {
                    attribute = der.constantPool.getEntryAs(attNameIndex, CPUTF8Entry.class).utf8String;
                } else {
                    throw new ClassFormatError("Index to attribute type " + attNameIndex + " not valid.");
                }

                long attLength = Integer.toUnsignedLong(in.readInt());

                switch (attribute) {
                    case "ConstantValue": //index to constant pool int entry with value of "final" field
                        int constIndex = Short.toUnsignedInt(in.readShort());
                        if (der.constantPool.entryIsType(constIndex, CPEntry.CONSTANT_Integer)) {
                            field.value = der.constantPool.getEntryAs(constIndex, CPIntEntry.class).constValue;
                        } else {
                            throw new ClassFormatError("Index to const value at " + constIndex + " not valid.");
                        }
                        break;

                    case "Code": //code attribute of a method, containing the bytecode instructions and additional information
                        if (method.hasCodeAttribute)
                            throw new ClassFormatError("Method " + method.name + " has more than one Code attribute.");
                        else
                            method.hasCodeAttribute = true;
                        method.maxStack = Short.toUnsignedInt(in.readShort());    //maximum stack depth
                        method.maxLocals = Short.toUnsignedInt(in.readShort());    //maximum number of local variables
                        method.codeLength = in.readInt();
                        if (method.codeLength >= 65536)
                            throw new ClassFormatError("Methods " + method.name + " code array must be shorter than 65536 entries.");

                        method.code = new byte[method.codeLength];
                        in.readNBytes(method.code, 0, method.codeLength);

                        //exception table, not used in TinyJava files
                        int exceptTableLength = Short.toUnsignedInt(in.readShort());
                        for (int j = 0; j < exceptTableLength; j++) {
                            for (int k = 0; k < 4; k++) {
                                in.readShort();
                            }
                        }
                        deriveAttributes(in, der, method, null);
                        break;

                    case "SourceFile": //index to a constant pool utf-8 entry with the name of the source file
                        int sourceIndex = Short.toUnsignedInt(in.readShort());
                        if (der.constantPool.entryIsType(sourceIndex, CPEntry.CONSTANT_Utf8)) {
                            der.sourceFileName = der.constantPool.getEntryAs(sourceIndex, CPUTF8Entry.class).utf8String;
                        } else {
                            throw new ClassFormatError("Index to source file name at " + sourceIndex + " not valid.");
                        }
                        break;

                    case "LineNumberTable": //corresponding line of code to program counter
                        int tableLength = Short.toUnsignedInt(in.readShort());
                        method.startPcs = new short[tableLength];
                        method.lineNumbers = new short[tableLength];
                        for (int j = 0; j < tableLength; j++) {
                            //get bytecode programme counters and associated lines in the source file
                            method.startPcs[j] = in.readShort();
                            method.lineNumbers[j] = in.readShort();
                        }
                        break;

                    default: //all other attributes get ignored
                        for (int j = 0; j < attLength; j++) {
                            in.readByte();
                        }
                }
            }
        } catch (IOException e) {
            throw new ClassFormatError("EOF while reading attributes (or other IO exception).");
        }
    }

    /**
     * While Format Checking (&sect;4.8) the {@code class} file, it must be made sure that the {@code class} file
     * doesn't have any extra bytes at the end.
     *
     * @param in the current input stream
     */
    private void checkEndOfFile(DataInputStream in) {
        try {
            in.readByte();
            throw new ClassFormatError("EOF not reached.");
        } catch (EOFException e) {
            //nothing, because verified :)
        } catch (IOException e) {
            throw new ClassFormatError("Other IO Error while reading EOF.");
        }
    }

    /**
     * Get the derived class representation for a class identifier. This should only be done,
     * if the class is already loaded, which can be checked with {@link tjvm.data.MethodArea#classLoaded(String)}.
     * The class loader doesn't contain a isLoaded method, because it should only be used to load classes,
     * whereas all other operations should be within the defined data areas of the JVM.
     * This method should only be used to link already loaded classes.
     *
     * @param className the name of the class (identifier)
     * @return the representation of the class
     */
    public DerivedClass getClass(String className) {
        return loadedClasses.get(className);
    }
}