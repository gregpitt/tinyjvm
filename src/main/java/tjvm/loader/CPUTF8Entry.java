package tjvm.loader;

/**
 * Class that represents a {@code CONSTANT_Utf8_info} structure in the constant pool. It only contains a String
 * that is used by other info structures (&sect;4.4.7).
 */
class CPUTF8Entry extends CPEntry {

    /** String of the entry. */
    String utf8String;

    /** Length of the String. */
    int length;

    /**
     * Create a new {@code CONSTANT_Utf8_info} structure for the constant pool.
     *
     * @param utf8String String of the entry
     */
	CPUTF8Entry(String utf8String) {
	    this.utf8String = utf8String;
	    length = utf8String.length();
    }

    @Override
    public String toString() {
        return "#" + cpIndex + " = Utf8\t\t" + utf8String;
    }

    @Override
    public void resolveIdentifier(ConstantPool constantPool) {
        identifier = utf8String;
    }
}