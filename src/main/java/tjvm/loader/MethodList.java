package tjvm.loader;

import java.util.Vector;

/**
 * Class that holds all information about the methods (&sect;4.6) of the {@code class} file after loading.
 * The methods are saved as {@link Method}s.
 */
public class MethodList {

    /**
     * Collection that holds all methods.
     */
    private Vector<Method> methList;

    /**
     * Create an empty list of methods.
     */
    MethodList() {
        methList = new Vector<>();
    }

    /**
     * Get the number of methods in the list.
     *
     * @return the number of methods
     */
    public int size() {
        return methList.size();
    }

    /**
     * Add a method to the method list.
     *
     * @param method the method to add
     * @throws InternalError if method is {@code null}
     */
    void addEntry(Method method) {
        if (method != null) {
            methList.add(method);
        } else {
            throw new InternalError("Error at MethodList.addMethTabEntry(): Given Method reference was null");
        }
    }

    /**
     * Get a method from the method list.
     *
     * @param index index of the entry to return
     * @return the entry at the specified index
     */
    public Method getEntry(int index) {
        return methList.get(index);
    }

    /**
     * Check if a method is contained in the method list.
     *
     * @param identifier the identifier of the method
     * @return {@code true} if method is in method list
     */
    boolean containsIdentifier(String identifier) {
        for (Method entry: methList)
            if (entry.identifier.equals(identifier))
                return true;
        return false;
    }

    /**
     * Returns the string representation of all methods, where every entry is indented.
     * The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of all methods
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (Method entry : methList)
            res.append(entry.toString()).append("\n\n");
        return res.toString();
    }
}