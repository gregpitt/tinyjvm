package tjvm.loader;

/**
 * Class that holds all information about a field (&sect;4.5) of the {@code class} file after loading.
 * The fields are saved in the {@link FieldList}.
 */
public class Field {

    /** The identifier which is used for easy identification of the field in the pattern of {@code Class.field:descriptor} */
    public String identifier;

    /** The name of the field. */
    String name;
    /** The descriptor of the field. For example "LArrayList;". */
    String descriptor;

    /** Shows, if the field is static. */
    public boolean isStatic;
    /** Shows, if the field is final. */
    public boolean isFinal;
    /** Representation of the {@code access_flags}. */
    short flags;

    /** The type of value of the field. This can be {@link ValueType#INT}, {@link ValueType#INTARR} or {@link ValueType#REF}.*/
    ValueType valueType;

    /** If the field is of type int and final, its value is stored in this variable.*/
    public int value;

    /**
     * Returns the string representation of a field where every line is indented.
     * The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of the field
     */
    @Override
    public String toString() {
        //first line
        StringBuilder res = new StringBuilder();
        res.append("\t");
        res.append(isStatic ? "static " : "");
        res.append(isFinal ? "final " : "");
        switch (valueType) {
            case INT:
                res.append("int ");
                break;
            case INTARR:
                res.append("int[] ");
                break;
            case REF:
                res.append(descriptor, 1, descriptor.length() - 1).append(" ");
                break;
        }
        res.append(name).append(";\n");

        //second line
        res.append("\tdescriptor: ").append(descriptor).append("\n");

        //third line
        res.append("\tflags: ").append(Integer.toBinaryString(flags));

        return res.toString();
    }

}