package tjvm.loader;

/**
 * Class that represents a {@code CONSTANT_Class_info} structure in the constant pool. It has an index
 * to a {@code CONSTANT_Utf8_info} structures that holds a class name (&sect;4.4.1).
 */
class CPClassEntry extends CPEntry {

    /** Index in constant pool to {@code CONSTANT_Utf8_info} structure with class name. */
    int classNameIndex;

    /**
     * Create a new {@code CONSTANT_Class_info} structure for the constant pool.
     *
     * @param classNameIndex index in constant pool to {@code CONSTANT_Utf8_info}structure with class name
     */
    CPClassEntry(int classNameIndex) {
	    this.classNameIndex = classNameIndex;
    }

    @Override
    public String toString() {
        return "#" + cpIndex + " = Class\t\t#" + classNameIndex + "\t\t // " + identifier;
    }

    @Override
    public void resolveIdentifier(ConstantPool constantPool) {
        identifier = constantPool.getEntryAs(classNameIndex, CPUTF8Entry.class).utf8String;
    }
}
