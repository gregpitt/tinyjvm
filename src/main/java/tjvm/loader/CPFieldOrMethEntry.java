package tjvm.loader;

/**
 * Class that represents a {@code CONSTANT_NameAndType_info} structure in the constant pool. It has indices
 * to a {@code CONSTANT_Class_info} structure, which holds information of the class of the method or field,
 * as well as to a {@code CONSTANT_NameAndType_info} structure, which holds the name and descriptor of a field
 * or method (&sect;4.4.2).
 */
class CPFieldOrMethEntry extends CPEntry {

    /** Index in constant pool to {@code CONSTANT_Class_info} structure with class name. */
    int classIndex;

    /** Index in constant pool to {@code CONSTANT_NameAndType_info} structure with name and descriptor. */
    int nameAndTypeIndex;

    /**
     * Create a new {@code CONSTANT_NameAndType_info} structure for the constant pool.
     *
     * @param classIndex index in constant pool to {@code CONSTANT_Class_info} structure with class name
     * @param nameAndTypeIndex index in constant pool to {@code CONSTANT_NameAndType_info} structure with name and descriptor
     */
    CPFieldOrMethEntry(int classIndex, int nameAndTypeIndex) {
        this.classIndex = classIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }

    @Override
    public String toString() {
        String type = tag == CONSTANT_Fieldref ? "Fieldref" : "Methodref";
        return "#" + cpIndex + " = " + type + "\t\t#" + classIndex + ".#" + nameAndTypeIndex + "\t\t // " + identifier;
    }

    @Override
    public void resolveIdentifier(ConstantPool constantPool) {
        CPNameAndTypeEntry nameAndType = constantPool.getEntryAs(nameAndTypeIndex, CPNameAndTypeEntry.class);
        CPClassEntry classEntry = constantPool.getEntryAs(classIndex, CPClassEntry.class);
        String className = constantPool.getEntryAs(classEntry.classNameIndex, CPUTF8Entry.class).utf8String;
        String name = constantPool.getEntryAs(nameAndType.nameIndex, CPUTF8Entry.class).utf8String;
        String descriptor = constantPool.getEntryAs(nameAndType.descriptorIndex, CPUTF8Entry.class).utf8String;
        identifier = className + "." + name + ":" + descriptor;
    }
}
