package tjvm;

import org.apache.commons.cli.*;
import tjvm.data.MethodArea;
import tjvm.execution.TinyJVM;
import tjvm.loader.ClassLoader;
import tjvm.loader.DerivedClass;
import tjvm.verifier.BytecodeVerifier;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Main class that represents a command line application for different usage scenarios using the {@link TinyJVM}
 * and its data structures to handle a JVM {@code class} file compiled from the sublanguage Tiny Java. <br>
 * The possible usages are:
 * <ul>
 * <li>Run the virtual machine with a class specified as main class that needs to have
 * the method {@code static void tinyMain()} implemented.</li>
 * <li>Decompile a class file similar to Oracle's Decompiler {@code javap} using the {@code -verbose} option.</li>
 * <li>Load a {@code class} file and create the class in the method area with printing the method area
 * afterwards.</li>
 * <li>Link a class from a {@code class} file and prepare the class in the method area with printing the
 * method area afterwards.</li>
 * <li>Initialize a class from a {@code class} file with printing the method area afterwards.</li>
 * <li>Initialize a class instance from a {@code class} file with printing the method area afterwards.</li>
 * </ul>
 * The command line application has a help that appears without giving any arguments or with the option {@code -help}.
 * Furthermore, errors like multiple arguments, no arguments etc. are printed if the console application is used
 * wrong.<br>
 * The only non-standard library used in the project, is the {@link org.apache.commons.cli} for the console application.
 */
class Main {

    /**
     * Main method that handles the options and, if necessary, the path to the {@code class} file to
     * start different paths of execution. Wrong inputs are handled appropriately like other
     * command line applications would do it.
     *
     * @param args options and path and name of the {@code class} file if necessary
     * @throws ParseException if anything unforeseen goes wrong
     */
    public static void main(String[] args) throws ParseException {
        //Create all options
        OptionGroup optionGroup = new OptionGroup();
        optionGroup.addOption(Option.builder("?").longOpt("help").
                desc("Print this usage message.").build());
        optionGroup.addOption(Option.builder("r").longOpt("run").hasArg().argName("classfile").
                desc("Run the static void tinyMain() method of the class file.").build());
        optionGroup.addOption(Option.builder("d").longOpt("decompile").hasArg().argName("classfile").
                desc("Print the decompiled classfile (like javap -v).").build());
        optionGroup.addOption(Option.builder("lo").longOpt("load").hasArg().argName("classfile").
                desc("Load the class from the classfile and print the method area.").build());
        optionGroup.addOption(Option.builder("li").longOpt("link").hasArg().argName("classfile").
                desc("Link the class from the classfile and print the method area.").build());
        optionGroup.addOption(Option.builder("ci").longOpt("classinit").hasArg().argName("classfile").
                desc("Initialize the class from the classfile and print the method area.").build());
        optionGroup.addOption(Option.builder("oi").longOpt("objectinit").hasArg().argName("classfile").
                desc("Initialize an instance of the class from the classfile and print the method area and heap.").build());
        optionGroup.setRequired(true);
        Options options = new Options();
        options.addOptionGroup(optionGroup);

        //Print help if no arguments specified
        if (args.length == 0) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("TinyJVM <option> <classfile>", options);
            System.exit(0);
        }

        //Try parsing and catch wrong input
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (MissingArgumentException e) {
            System.out.println("Error: no classfile or more than one option specified \n" +
                    "usage: TinyJVM <option> <classfile> \n" +
                    "use -help for a list of possible options");
            System.exit(-1);
        } catch (MissingOptionException e) {
            System.out.println("Error: no option specified \n" +
                    "usage: TinyJVM <option> <classfile> \n" +
                    "use -help for a list of possible options");
            System.exit(-1);
        } catch (UnrecognizedOptionException e) {
            System.out.println("Error: unknown option " + args[0] + "\n" +
                    "usage: TinyJVM <option> <classfile> \n" +
                    "use -help for a list of possible options");
            System.exit(-1);
        } catch (AlreadySelectedException e) {
            System.out.println("Error: please specify only one option\n" +
                    "usage: TinyJVM <option> <classfile> \n" +
                    "use -help for a list of possible options");
            System.exit(-1);
        }

        //Print help message (only option without arguments)
        if (cmd.hasOption("?")) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("TinyJVM <option> <classfile>", options);
            System.exit(0);
        }

        //Get file path etc (if other options used)
        Path file = Paths.get(cmd.getOptions()[0].getValue());
        String fileName = file.getFileName().toString().replace(".class", "");
        String pathName;
        try {
            pathName = file.getParent().toString();
        } catch (NullPointerException e) {
            pathName = "";
        }

        //Run depending on option
        if (cmd.hasOption('r')) {
            run(pathName, fileName);
        } else if (cmd.hasOption("d")) {
            decompile(pathName, fileName);
        } else if (cmd.hasOption("lo")) {
            load(pathName, fileName);
        } else if (cmd.hasOption("li")) {
            link(pathName, fileName);
        } else if (cmd.hasOption("ci")) {
            classInitialize(pathName, fileName);
        } else if (cmd.hasOption("oi")) {
            objectInitialize(pathName, fileName);
        }
    }

    /**
     * Run the {@code static void tinyMain()} method that is located in the specified
     * {@code class} file. The {@link TinyJVM} does all steps necessary before that,
     * that is starting itself and loading, linking and initializing the class
     * from the file.<br>
     * The execution stops, if there is an error thrown or if there are no further
     * instructions to be executed (because the main method ends).
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void run(String pathName, String fileName) {
        TinyJVM jvm = new TinyJVM();
        try {
            jvm.start(pathName, fileName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        jvm.run();
    }

    /**
     * Decompile a {@code class} file and print its contents on the command line. The
     * output looks similar to Oracle's Decompiler {@code javap} using the {@code -verbose} option.
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void decompile(String pathName, String fileName) {
        try {
            System.out.println((new ClassLoader(pathName).load(fileName).toString()));
        } catch (ClassNotFoundException e) {
            System.out.println("Class " + pathName + fileName + " not found in the file system.");
            System.exit(-1);
        }
    }

    /**
     * Load the class of a {@code class} file and create the class in the method area. The
     * method area is printed to the command line after creation.
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void load(String pathName, String fileName) {
        try {
            DerivedClass dc = new ClassLoader(pathName).load(fileName);
            MethodArea ma = new MethodArea();
            ma.create(dc);
            System.out.println(ma.toString());
        } catch (ClassNotFoundException e) {
            System.out.println("Class " + pathName + fileName + " not found in the file system.");
            System.exit(-1);
        }

    }

    /**
     * Load the class of a {@code class} file and create the class in the method area. Afterwards
     * the class is verified and prepared in the method area. The method area is printed to the
     * command line after preparation.
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void link(String pathName, String fileName) {
        try {
            DerivedClass dc = new ClassLoader(pathName).load(fileName);
            MethodArea ma = new MethodArea();
            ma.create(dc);
            new BytecodeVerifier().verify(dc);
            ma.prepare(dc);
            System.out.println(ma.toString());
        } catch (ClassNotFoundException e) {
            System.out.println("Class " + pathName + fileName + " not found in the file system.");
            System.exit(-1);
        }

    }

    /**
     * Load the class of a {@code class} file and create the class in the method area. Afterwards
     * the class is verified and prepared in the method area. Then the class is initialized, which
     * consists of setting its {@code static final} fields to their values and running the
     * {@code <clinit>} method. The method area is printed to the command line after initialization.
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void classInitialize(String pathName, String fileName) {
        TinyJVM jvm = new TinyJVM();
        jvm.initClassAndPrint(pathName, fileName, true);
    }

    /**
     * Initialize the class of a {@code class} file. Afterwards, the {@code <init>} method of the
     * class is run for the corresponding object, that is created in the heap. After running the method,
     * the. The method area is printed to the command line after initialization.
     *
     * @param pathName the path of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    private static void objectInitialize(String pathName, String fileName) {
        TinyJVM jvm = new TinyJVM();
        jvm.initObjectAndPrint(pathName, fileName);
    }

}