package tjvm.data;

import tjvm.loader.CPEntry;
import tjvm.loader.DerivedClass;
import tjvm.loader.Field;
import tjvm.loader.Method;
import tjvm.verifier.BytecodeVerifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * The {@code method area} is a Run-Time Data Area defined by "The Java Virtual Machine Specification" (Java SE 10 Edition)
 * in chapter &sect;2.5.4. It is shared among all Java Virtual Machine threads and is supposed to store per-class
 * structures such as run-time constant pool, field and method data and code for methods and constructors
 * While there are no specifications on how the implementation of the method area has to be,
 * this implementation is based roughly on the data structures of the lecture "Mobiler Code" of Prof. Wolfram Amme at
 * the FSU Jena. These data structures follow the principle of addressing data with a base- and offset-addresses.
 * Furthermore there are lookup-tables in which offsets for methods and fields are saved. The structures are:
 * <ul>
 * <li>{@link #classDescriptionTable}: data structure to store per class information for interpreter and compiler.</li>
 * <li>{@link #methodTable}: data structure to store offsets for methods of classes.</li>
 * <li>{@link #byteCodeTable}: data structure to store the bytecode methods of classes.</li>
 * <li>{@link #offsetTableClass}: data structure to store offsets for classes.</li>
 * <li>{@link #offsetTableField}: data structure to store offsets for fields of classes.</li>
 * <li>{@link #offsetTableMethod}: data structure to store offsets for methods of classes.</li>
 * <li>{@link #runtimeConstantPools}: data structure representing the information in the {@code constant pool}s from
 * a {@code class} file (&sect;4.4).</li>
 * <li>{@link #finalFieldList}: Set for lookup, if a field is final.</li>
 * </ul>
 * The data structures are implemented with as little additional information as possible. For example, the
 * {@link #classDescriptionTable} is implemented using a {@link DynamicIntArray}, which is only an {@code int[]}-array
 * with a wrapper class, that extends the size  the array, when necessary and a few methods for easy getting and
 * setting. <br>of
 * Furthermore, the method area has to do the {@code Creation} (&sect;5.3), which is the construction of a class in the
 * method area. To make this step more transparent and not mixed up in the {@code Loading}, which is done by the
 * {@link tjvm.loader.ClassLoader}. For the creation, the method area is passed an instance of {@link DerivedClass},
 * which functions as an intermediate representation of a loaded, but not yet created classes. <br>
 * With this {@link DerivedClass}, not only the method {@link #create(DerivedClass)}, but also
 * {@link #prepare(DerivedClass)} and {@link #initializeConstantFields(DerivedClass)}are called. <br>
 * <i>Future Versions: Store constants that are now in the Runtime constant pool for compiled code.</i>
 */
public class MethodArea {

    /**
     * Default value for not yet initialized offsets.
     */
    private static final int DEFAULT_OFFSET = Integer.MIN_VALUE;
    /**
     * Default value for {@code int} values and references to arrays or objects.
     */
    private static final int DEFAULT_VALUE = 0;

    /**
     * The length of the {@code Class Descriptor} (CD) of a class.
     */
    private static final int CD_LENGTH = 5;
    /**
     * The offset of the entry containing the number of fields of a class in the {@code Class Descriptor}.
     */
    private static final int CD_FIELD_COUNT = -1;
    /**
     * The offset of the entry containing the reference to the {@code Method Table}
     * of a class in the {@code Class Descriptor}.
     */
    private static final int CD_METHOD_TABLE = -2;
    /**
     * The offset of the entry containing the reference to the {@code Runtime Constant Pool}
     * of a class in the {@code Class Descriptor}.
     */
    private static final int CD_RCP = -3;
    /**
     * The offset of the entry containing the reference to the {@code Class Descriptor} to the super class
     * of a class in the {@code Class Descriptor}.
     */
    private static final int CD_SUPER_CLASS = -4;
    /**
     * The offset of the entry containing the status of a class in the {@code Class Descriptor}.
     */
    private static final int CD_STATUS = -5;

    /**
     * Status of a class that it is loaded (&sect; 5.3).
     */
    private static final int STATUS_LOADED = 0;
    /**
     * Status of a class that it is linked (and loaded) (&sect; 5.4).
     */
    private static final int STATUS_LINKED = 1;
    /**
     * Status of a class that is being initialized at the moment. (&sect; 5.5).
     */
    private static final int STATUS_INITIALIZING = 2;
    /**
     * /**
     * Status of a class that is initialized (&sect; 5.5).
     */
    private static final int STATUS_INITIALIZED = 3;


    /**
     * The number of {@code bytes} of the whole info-structure for every Bytecode method in the {@link #byteCodeTable}.
     */
    private static final int BC_LENGTH = 9;
    /**
     * The offset of the {@code code_length} from the base of the method in the {@link #byteCodeTable}.
     */
    private static final int BC_BYTE_COUNT = -4;
    /**
     * The number of {@code bytes} for the {@code code_length} of the code array.
     */
    private static final int BC_BYTE_COUNT_SIZE = 4;
    /**
     * The offset of the {@code max_locals} from the base of the method in the {@link #byteCodeTable}.
     */
    private static final int BC_LOCALS = -6;
    /**
     * The number of {@code bytes} for the {@code max_locals} of the code array.
     */
    private static final int BC_LOCALS_SIZE = 2;
    /**
     * The offset of the {@code max_stack} from the base of the method in the {@link #byteCodeTable}.
     */
    private static final int BC_STACK = -8;
    /**
     * The number of {@code bytes} for the {@code max_stack} of the code array.
     */
    private static final int BC_STACK_SIZE = 2;
    /**
     * The offset of the {@code args_size} from the base of the method in the {@link #byteCodeTable}.
     */
    private static final int BC_ARGS = -9;
    /**
     * The number of {@code bytes} for the {@code args_size} of the method.
     */
    private static final int BC_ARGS_SIZE = 1;

    /**
     * Data structure to store per class information that is used by interpreter and compiler such as
     * the Class Descriptor (CD), static fields, objects and arrays.
     */
    private DynamicIntArray classDescriptionTable;
    /**
     * Data structure to store the offsets of the methods. It is usually used for polymorphism.
     */
    private DynamicIntArray methodTable;
    /**
     * Data structure to store the non-compiled byte code of the methods of classes.
     */
    private DynamicByteArray byteCodeTable;

    /**
     * Data structure to store offsets for methods of classes.
     */
    private HashMap<String, Integer> offsetTableMethod;
    /**
     * Data structure to store offsets for fields of classes.
     */
    HashMap<String, Integer> offsetTableClass;
    /**
     * Data structure to store offsets for fields of classes.
     */
    HashMap<String, Integer> offsetTableField;

    /**
     * Data structure representing the information in the {@code constant pool}s from the {@code class} files (&sect;4.4).
     */
    private ArrayList<RuntimeCP> runtimeConstantPools;

    /**
     * List of all final fields, which must be checked during interpretation of the code.
     */
    private HashSet<String> finalFieldList;

    /**
     * Create a new array method area. All data structures are created empty.
     */
    public MethodArea() {
        classDescriptionTable = new DynamicIntArray();
        methodTable = new DynamicIntArray();
        byteCodeTable = new DynamicByteArray();
        runtimeConstantPools = new ArrayList<>();
        offsetTableClass = new HashMap<>();
        offsetTableField = new HashMap<>();
        offsetTableMethod = new HashMap<>();
        finalFieldList = new HashSet<>();
    }

    /**
     * Method to create a class in the method area.
     * <blockquote>"Creation of a class [...] consists of the construction in the method area of the Java Virtual
     * Machine (&sect;2.5.4) of an implementation-specific internal representation" (&sect;5.3 JVMS)</blockquote>
     * In "The Java Virtual Machine Specification" the process of creation is mixed with loading, whereas in this
     * implementation, the loading and creation are separated on purpose to make the used internal data structures
     * better replaceable. <br>
     * In this array-like implementation of the {@link MethodArea}, the creation contains the following steps:
     * <ol>
     * <li>Creation of the {@link RuntimeCP} in the {@link #runtimeConstantPools}</li>
     * <li>Creating the entries in {@link #offsetTableField} while setting static fields to default value
     * and counting non-static fields.</li>
     * <li>Creating the entries in {@link #offsetTableMethod} while setting static methods to default value
     * and reserving space for non-static methods in the {@link #methodTable}.</li>
     * <li>Creating the {@code Class Descriptor} in the {@link #classDescriptionTable} with the number of non-static
     * fields, the base-offset to the {@link #offsetTableMethod}, the offset to entry in the {@link #runtimeConstantPools}
     * and the offset to the {@code Class Descriptor} of the super class.</li>
     * <li>Creating the entry in the {@link #offsetTableClass} which is an offset to the
     * {@code Class Descriptor}.</li>
     * </ol>
     *
     * @param dc The derived class from the {@link tjvm.loader.ClassLoader}.
     */
    public void create(DerivedClass dc) {
        //1. Runtime Constant Pool
        RuntimeCP rcp = new RuntimeCP();
        for (int i = 1; i <= dc.constantPool.size(); i++) {
            CPEntry oldEntry = dc.constantPool.getEntry(i);
            RuntimeCPEntry newEntry = new RuntimeCPEntry();
            newEntry.copyValuesFrom(oldEntry);
            rcp.addEntry(newEntry);
        }
        int rcpIndex = runtimeConstantPools.size();
        runtimeConstantPools.add(rcp);
        dc.runtimeCP = rcp; //--> easier handling in verifier

        //2. Offset Table Field
        int fieldCount = 0;
        for (int i = 0; i < dc.fieldList.size(); i++) {
            Field field = dc.fieldList.getEntry(i);
            if (field.isStatic) {
                offsetTableField.put(field.identifier, DEFAULT_OFFSET); //default offset --> Initialization while linking
            } else {
                //could be hidden in heap, but this shows more how it works
                offsetTableField.put(field.identifier, -1 - Heap.INFO_LENGTH - fieldCount);
                fieldCount++;
            }
            if (field.isFinal) {
                finalFieldList.add(field.identifier);
            }
        }

        //3. Offset Table Methods
        int methodCount = 0;
        for (int i = 0; i < dc.methodList.size(); i++) {
            Method method = dc.methodList.getEntry(i);
            if (method.isStatic) {
                offsetTableMethod.put(method.identifier, DEFAULT_OFFSET); //default offset --> Initialization while linking
            } else {
                methodTable.addElement(DEFAULT_OFFSET); //default offset --> Initialization while linking
                offsetTableMethod.put(method.identifier, -1 - methodCount);
                methodCount++;
            }
        }
        int mtOffset = methodTable.getPointerPosition();

        //4. Class Descriptor
        classDescriptionTable.addElements(new int[CD_LENGTH]);
        int cdBase = classDescriptionTable.getPointerPosition();
        classDescriptionTable.setElement(cdBase + CD_FIELD_COUNT, fieldCount);
        classDescriptionTable.setElement(cdBase + CD_METHOD_TABLE, mtOffset);
        classDescriptionTable.setElement(cdBase + CD_RCP, rcpIndex);
        classDescriptionTable.setElement(cdBase + CD_SUPER_CLASS, DEFAULT_OFFSET); //default offset, because no super class
        classDescriptionTable.setElement(cdBase + CD_STATUS, STATUS_LOADED);

        //5. Offset Table Class
        offsetTableClass.put(dc.className, cdBase);
    }

    /**
     * Method to prepare a class in the method area which is a part of linking.
     * <blockquote>
     * "Preparation involves creating the static fields for a class […] and initializing such fields
     * to their default values (&sect;2.3, &sect;2.4). This does not require the execution of any
     * Java Virtual Machine code;explicit initializers for static fields are executed as part of
     * initialization (&sect;5.5), not preparation." (&sect;5.4 JVMS)</blockquote>
     * In this implementation, the preparation in the method area is supposed to be done only for {@link DerivedClass}es,
     * that are already verified by the {@link BytecodeVerifier}
     * In this array-like implementation of the {@link MethodArea}, the preparation contains the following steps:
     * <ol>
     * <li>Saving the bytecode in the {@link #byteCodeTable} while doing the following:
     * <ul>
     * <li>For static methods: Allocating memory in the {@link #classDescriptionTable}, setting the offsets to the
     * right position in the {@link #byteCodeTable} and updating the offsets in the
     * {@link #offsetTableMethod}.</li>
     * <li>For non-static methods: Setting the offsets of the {@link #methodTable} to the
     * right position in the {@link #byteCodeTable}.</li>
     * </ul>
     * </li>
     * <li>Allocating memory in {@link #classDescriptionTable} for static fields and setting them to default values while
     * updating the offsets in {@link #offsetTableField}.</li>
     * </ol>
     *
     * @param dc The derived and already verified class from the {@link tjvm.loader.ClassLoader}.
     */
    public void prepare(DerivedClass dc) {
        //1. Saving the Bytecode
        int cdBase = offsetTableClass.get(dc.className);
        int mtOffset = classDescriptionTable.getElement(cdBase + CD_METHOD_TABLE);

        for (int i = 0; i < dc.methodList.size(); i++) {
            Method method = dc.methodList.getEntry(i);

            //set Bytecode
            byteCodeTable.addElements(new byte[BC_LENGTH]);
            int base = byteCodeTable.addElements(method.code);

            //Set info-fields
            byte[] bytecodeSize = ByteConverter.toBytes(BC_BYTE_COUNT_SIZE, method.code.length);
            byteCodeTable.setElements(bytecodeSize, base + BC_BYTE_COUNT);
            byte[] maxLocals = ByteConverter.toBytes(BC_LOCALS_SIZE, method.maxLocals);
            byteCodeTable.setElements(maxLocals, base + BC_LOCALS);
            byte[] maxStack = ByteConverter.toBytes(BC_STACK_SIZE, method.maxStack);
            byteCodeTable.setElements(maxStack, base + BC_STACK);
            byte[] argsSize = ByteConverter.toBytes(BC_ARGS_SIZE, method.argsSize);
            byteCodeTable.setElements(argsSize, base + BC_ARGS);

            //put offset in in class description table or method table
            if (method.isStatic) {
                int staticOffset = classDescriptionTable.addElement(base);
                offsetTableMethod.replace(method.identifier, staticOffset);
            } else {
                int methodOffset = offsetTableMethod.get(method.identifier);
                methodTable.setElement(mtOffset + methodOffset, base);
            }
        }

        //2. Offset Table Field
        for (int i = 0; i < dc.fieldList.size(); i++) {
            Field field = dc.fieldList.getEntry(i);
            if (field.isStatic) {
                int offset = classDescriptionTable.addElement(DEFAULT_VALUE);
                offsetTableField.replace(field.identifier, offset);
            }
        }

        //3. Mark as linked
        classDescriptionTable.setElement(cdBase + CD_STATUS, STATUS_LINKED);
    }

    /**
     * Method to set the static final fields. If the field doesn't have a ConstantValue
     * attribute, this just leaves the default-value {@code 0}.
     * This is a part of initialization, that is done by the Tiny JVM.
     *
     * @param dc the derived class with the information about static final fields
     */
    public void initializeConstantFields(DerivedClass dc) {
        for (int i = 0; i < dc.fieldList.size(); i++) {
            Field field = dc.fieldList.getEntry(i);
            if (field.isStatic && field.isFinal) {
                int offset = offsetTableField.get(field.identifier);
                setClassDescTableEntry(offset, field.value);
            }
        }
    }

    /**
     * Get a runtime constant pool entry of a specific class.
     *
     * @param classIndex the index of the class in the runtime constant pool
     * @param poolIndex  the constant pool index within the constant pool of that class
     * @return the specified runtime constant pool entry
     */
    public RuntimeCPEntry getCPEntryAt(int classIndex, int poolIndex) {
        return runtimeConstantPools.get(classIndex).getEntry(poolIndex);
    }

    /**
     * Get the offset for a class descriptor. The offset must not be negative.
     *
     * @param identifier string representation of the class
     * @return the offset for the class
     */
    public int getClassOffsetFor(String identifier) {
        return offsetTableClass.get(identifier);
    }

    /**
     * Check, if a class is already loaded. This is the case, if the {@link #offsetTableClass} already
     * contains the class name.
     *
     * @param className the name of the class
     * @return {@code true} if the class is already loaded
     */
    public boolean classLoaded(String className) {
        return offsetTableClass.containsKey(className);
    }

    /**
     * Check, if a class is already linked. This is the case, if the value {@link #CD_STATUS} is
     * of value {@link #STATUS_LINKED} or {@link #STATUS_INITIALIZED}.
     *
     * @param className the name of the class
     * @return {@code true} if the class is already linked
     */
    public boolean classLinked(String className) {
        if (!offsetTableClass.containsKey(className))
            return false;
        int classOffset = offsetTableClass.get(className);
        return getClassDescTableEntry(classOffset + CD_STATUS) >= STATUS_LINKED;
    }

    /**
     * Set a class to be initialized in the {@code Class Descriptor} in the field {@link #CD_STATUS}.
     *
     * @param className the name of the class
     */
    public void setInitialized(String className) {
        int classOffset = offsetTableClass.get(className);
        setClassDescTableEntry(classOffset + CD_STATUS, STATUS_INITIALIZED);
    }

    /**
     * Set a class to be during initializing in the {@code Class Descriptor} in the field {@link #CD_STATUS}.
     *
     * @param className the name of the class
     */
    public void setInitializing(String className) {
        int classOffset = offsetTableClass.get(className);
        setClassDescTableEntry(classOffset + CD_STATUS, STATUS_INITIALIZING);
    }


    /**
     * Check, if a class is during initializing or already initialized. This is the case, if the value
     * {@link #CD_STATUS} is of value {@link #STATUS_INITIALIZED} or {@link #STATUS_INITIALIZING}.
     *
     * @param className the name of the class
     * @return {@code true} if the class is already linked
     */
    public boolean isInitializing(String className) {
        if (!offsetTableClass.containsKey(className))
            return false;
        int classOffset = offsetTableClass.get(className);
        return getClassDescTableEntry(classOffset + CD_STATUS) >= STATUS_INITIALIZING;
    }

    /**
     * Get the offset for a field. The offset can be negative or positive, depending on whether the field is static
     * or not.
     *
     * @param identifier string representation of the field after the pattern {@code Class.field:descriptor}
     * @return the offset for the field
     */
    public int getFieldOffsetFor(String identifier) {
        return offsetTableField.get(identifier);
    }

    /**
     * Get the offset for a method. The offset can be negative or positive, depending on whether the method is
     * already verified or not.
     *
     * @param identifier string representation of the method after the pattern {@code Class.method:descriptor}
     * @return the offset for the method
     */
    public int getMethodOffsetFor(String identifier) {
        return offsetTableMethod.get(identifier);
    }

    /**
     * Get the value of the class description table for an offset. This should usually be used to get
     * static fields or the offsets for static methods.
     *
     * @param offset the offset of the class table entry
     * @return the value of class table entry
     */
    public int getClassDescTableEntry(int offset) {
        return classDescriptionTable.getElement(offset);
    }

    /**
     * Set the value of the class description tablefor an offset.
     *
     * @param offset the offset of the class table entry
     * @param value  the value of class table entry
     */
    public void setClassDescTableEntry(int offset, int value) {
        classDescriptionTable.setElement(offset, value);
    }

    /**
     * Get the index to the {@link RuntimeCP} for a class.
     *
     * @param cdBase the base of the {@code Class Descriptor}
     * @return the index to the {@code Runtime Constant Pool}
     */
    public int getRCPIndex(int cdBase) {
        return getClassDescTableEntry(cdBase + CD_RCP);
    }

    /**
     * Get the number of fields for new instances of a class.
     *
     * @param cdBase the base of the {@code Class Descriptor}
     * @return the number of fields for new instances of that class
     */
    public int getFieldCount(int cdBase) {
        return getClassDescTableEntry(cdBase + CD_FIELD_COUNT);
    }

    /**
     * Get the offset to the method offsets of a class in the {@link #methodTable}.
     *
     * @param cdBase the base of the {@code Class Descriptor}
     * @return the offset in the {@link #methodTable}
     */
    public int getMethodTableOffset(int cdBase) {
        return getClassDescTableEntry(cdBase + CD_METHOD_TABLE);
    }

    /**
     * Get the value of the method table for an offset.
     *
     * @param offset the offset of the method table entry
     * @return the value of method table entry
     */
    public int getMethodTableEntry(int offset) {
        return methodTable.getElement(offset);
    }

    /**
     * Get a single {@code byte} of the Bytecode for a specified offset.
     *
     * @param offset the offset in the Bytecode array
     * @return the single {@code byte}
     */
    public byte getBytecode(int offset) {
        return byteCodeTable.getElement(offset);
    }

    /**
     * Get the the {@code max_locals} of a bytecode method for a specified offset.
     *
     * @param offset the offset of the bytecode
     * @return the maximum local variables for the method
     */
    public int getMaxLocals(int offset) {
        byte[] localsArray = byteCodeTable.getElements(offset + BC_LOCALS, BC_LOCALS_SIZE);
        return ByteConverter.toUnsignedInt(localsArray);
    }

    /**
     * Get the {@code max_stack} of a method for a specified offset.
     *
     * @param offset the offset of the bytecode
     * @return the maximum stack height for the method
     */
    public int getMaxStack(int offset) {
        byte[] stackArray = byteCodeTable.getElements(offset + BC_STACK, BC_STACK_SIZE);
        return ByteConverter.toUnsignedInt(stackArray);
    }

    /**
     * Get the {@code args_size} of a method for a specified offset.
     *
     * @param offset the offset of the bytecode
     * @return the maximum stack height for the method
     */
    public int getArgsSize(int offset) {
        byte[] argsArray = byteCodeTable.getElements(offset + BC_ARGS, BC_ARGS_SIZE);
        return ByteConverter.toUnsignedInt(argsArray);
    }

    /**
     * Get the {@code code_length} of a method for a specified offset.
     *
     * @param offset the offset of the bytecode
     * @return the maximum stack height for the method
     */
    public int getCodeLength(int offset) {
        byte[] lengthArray = byteCodeTable.getElements(offset + BC_BYTE_COUNT, BC_BYTE_COUNT_SIZE);
        return ByteConverter.toUnsignedInt(lengthArray);
    }

    /**
     * Check, if a field is final.
     *
     * @param identifier the identifier of the field
     * @return {@code true} if the field is declared final
     */
    public boolean isFinalField(String identifier) {
        return finalFieldList.contains(identifier);
    }


    /**
     * Returns an overview over the whole method area. All data structures are listed one after another.
     * This method is more a decompiler of the contents than just a simple printer.
     *
     * @return the string representation of the method area
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n--------- METHOD AREA ---------\n");

        builder.append("--- Runtime Constant Pools ---\n");
        for (int i = 0; i < runtimeConstantPools.size(); i++) {
            builder.append("Runtime Constant Pool ").append(i + 1).append("\n");
            builder.append(runtimeConstantPools.get(i).toString()).append("\n");
        }

        builder.append("\n--- Bytecode Table ---\n");
        HashMap<Integer, String> codeDesc = new HashMap<>();
        for (Map.Entry<String, Integer> entry : offsetTableMethod.entrySet()) {
            if (entry.getValue() == DEFAULT_OFFSET)
                continue;
            int codeBase;
            String methId = entry.getKey();
            if (entry.getValue() < 0) {
                String classId = methId.substring(0, methId.indexOf("."));
                int cdBase = getClassOffsetFor(classId);
                codeBase = getMethodTableEntry(getMethodTableOffset(cdBase) + entry.getValue());
            } else {
                codeBase = getClassDescTableEntry(entry.getValue());
            }
            if (codeBase == DEFAULT_OFFSET)
                continue;
            codeDesc.put(codeBase + BC_ARGS, "(" + methId + " -- Header: args [1byte] : " + getArgsSize(codeBase) + ")");
            codeDesc.put(codeBase + BC_STACK, "(" + methId + " -- Header: max_stack [2bytes] : " + getMaxStack(codeBase) + ")");
            codeDesc.put(codeBase + BC_LOCALS, "(" + methId + " -- Header: max_locals [2bytes] : " + getMaxLocals(codeBase) + ")");
            codeDesc.put(codeBase + BC_BYTE_COUNT, "(" + methId + " -- Header: code_length [4bytes] : " + getCodeLength(codeBase) + ")");
            codeDesc.put(codeBase, "(" + methId + " -- Beginning of Bytecode)");
        }
        for (int i = 0; i < byteCodeTable.getPointerPosition(); i++) {
            String desc = codeDesc.getOrDefault(i, "");
            builder.append("#").append(i).append(": ").append(Byte.toUnsignedInt(byteCodeTable.getElement(i))).
                    append("\t").append(desc).append("\n");
        }

        builder.append("\n--- Class Description Table ---\n");
        HashMap<Integer, String> classDesc = new HashMap<>();
        for (Map.Entry<String, Integer> entry : offsetTableClass.entrySet()) {
            String className = entry.getKey();
            classDesc.put(entry.getValue() + CD_STATUS, "(" + className + " -- CD: status)");
            classDesc.put(entry.getValue() + CD_SUPER_CLASS, "(" + className + " -- CD: super class)");
            classDesc.put(entry.getValue() + CD_RCP, "(" + className + " -- CD: rcp index)");
            classDesc.put(entry.getValue() + CD_METHOD_TABLE, "(" + className + " -- CD: method table)");
            classDesc.put(entry.getValue() + CD_FIELD_COUNT, "(" + className + " -- CD: field count)");
        }
        for (Map.Entry<String, Integer> entry : offsetTableField.entrySet()) {
            if (entry.getValue() > 0)
                classDesc.put(entry.getValue(), "(" + entry.getKey() + " -- static field)");
        }
        for (Map.Entry<String, Integer> entry : offsetTableMethod.entrySet()) {
            if (entry.getValue() > 0)
                classDesc.put(entry.getValue(), "(" + entry.getKey() + " -- static method)");
        }
        for (int i = 0; i < classDescriptionTable.getPointerPosition(); i++) {
            String desc = classDesc.getOrDefault(i, "");
            builder.append("#").append(i).append(": ").append(classDescriptionTable.getElement(i)).
                    append("\t\t").append(desc).append("\n");
        }

        builder.append("\n--- Method Table ---\n");
        HashMap<Integer, String> methDesc = new HashMap<>();
        for (Map.Entry<String, Integer> entry : offsetTableMethod.entrySet()) {
            if (entry.getValue() < 0) {
                String methId = entry.getKey();
                String classId = methId.substring(0, methId.indexOf("."));
                int cdBase = getClassOffsetFor(classId);
                int methTabOffset = getMethodTableOffset(cdBase);
                methDesc.put(entry.getValue() + methTabOffset, "(" + entry.getKey() + ")");
            }
        }
        for (int i = 0; i < methodTable.getPointerPosition(); i++) {
            String desc = methDesc.getOrDefault(i, "");
            builder.append("#").append(i).append(": ").append(methodTable.getElement(i)).
                    append("\t").append(desc).append("\n");
        }

        builder.append("\n--- Offset Table Class ---\n");
        for (HashMap.Entry<String, Integer> entry : offsetTableClass.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }

        builder.append("\n--- Offset Table Field ---\n");
        for (HashMap.Entry<String, Integer> entry : offsetTableField.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }

        builder.append("\n--- Offset Table Method ---\n");
        for (HashMap.Entry<String, Integer> entry : offsetTableMethod.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }

        builder.append("\n--- Final Fields List ---\n");
        for (String entry : finalFieldList) {
            builder.append(entry).append("\n");
        }

        return builder.toString();
    }
}