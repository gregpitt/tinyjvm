package tjvm.data;

import tjvm.loader.CPEntry;
import tjvm.loader.ConstantPool;

/**
 * A runtime constant pool entry is a part of the {@code Runtime Constant Pool} (&sect;5.1), which holds
 * all constant pool entries of a loaded class and is used for resolving the symbolic references
 * to classes, method and fields.
 * Each entry has a specific type which can be already resolved to an offset of that class,
 * method or field.
 */
public class RuntimeCPEntry extends CPEntry {

    /**
     * {@code true}, if Resolution (&sect;5.4.3) has already been done for that entry.
     */
    public boolean resolved;

    /**
     * If the entry is already resolved, the offset points to the field, method or class.
     */
    public int offset;

    /**
     * Create a new entry, that is not resolved yet with {@link #offset} set to {@code 0}.
     */
    RuntimeCPEntry() {
        resolved = false;
        offset = 0;
    }


    @Override
    public void resolveIdentifier(ConstantPool constantPool) {
        //nothing to do in this case
    }

    /**
     * If the {@link #identifier} has a part with the name of the class, that part is returned. For example:
     * <code><u>Class</u>.method:descriptor</code>
     *
     * @return the substring for the class
     */
    public String getClassSubstring() {
        if (identifier.contains("."))
            return identifier.substring(0, identifier.indexOf("."));
        else
            return identifier;
    }

    /**
     * If the {@link #identifier} has a part with the name of the method or field, that part is returned. For example:
     * <code>Class.<u>method</u>:descriptor</code>
     *
     * @return the substring for the class
     */
    public String getNameSubstring() {
        if (identifier.contains(".") && identifier.contains(":"))
            return identifier.substring(identifier.indexOf(".") + 1, identifier.indexOf(":"));
        else
            return identifier;
    }

    /**
     * If the {@link #identifier} has a part with the descriptor of the method or field, that part is returned. For example:
     * <code>Class.method:<u>descriptor</u></code>
     *
     * @return the substring for the class
     */
    public String getDescriptorSubstring() {
        if (identifier.contains(":"))
            return identifier.substring(identifier.indexOf(":"));
        else
            return identifier;
    }


    /**
     * Copy the values {@link CPEntry#cpIndex}, {@link CPEntry#tag},{@link CPEntry#identifier}
     * and {@link CPEntry#constValue} to the fields of the instance of {@link RuntimeCPEntry}. <p>
     * Before calling this method, {@link CPEntry#resolveIdentifier(ConstantPool)} should be called
     * on the entry to copy the values from.
     *
     * @param oldEntry the entry to copy values from
     */
    void copyValuesFrom(CPEntry oldEntry) {
        cpIndex = oldEntry.cpIndex;
        tag = oldEntry.tag;
        identifier = oldEntry.identifier;
        constValue = oldEntry.constValue;
    }

}
