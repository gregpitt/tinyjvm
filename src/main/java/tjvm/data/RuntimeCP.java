package tjvm.data;

import java.util.Vector;

/**
 * The {@code Runtime Constant Pool} holds all constant pool entries of a loaded class.
 * It is used for resolving the symbolic references to classes, method and fields and is
 * described in  "The Java Virtual Machine Specification" (Java SE 10 Edition) in &sect;5.1) as follows.
 * <blockquote>
 * "The Java Virtual Machine maintains a per-type constant pool (&sect;2.5.5), a run-time
 * data structure that serves many of the purposes of the symbol table of a conventional
 * programming language implementation. <p>
 * The {@code constant_pool} table (&sect;4.4) in the binary representation of a class or interface
 * is used to construct the run-time constant pool upon class or interface creation
 * (&sect;5.3). All references in the run-time constant pool are initially symbolic. The
 * symbolic references in the run-time constant pool are derived from structures in
 * the binary representation of the class or interface[...]
 * </blockquote>
 */
public class RuntimeCP {

    /**
     * Vector of all entries of the constant pool.
     */
    private Vector<RuntimeCPEntry> cpEntries;

    /**
     * Create an empty constant pool.
     */
    RuntimeCP() {
        cpEntries = new Vector<>();
    }

    /**
     * Add an entry to the constant pool. Entries to the constant pool should be added in their specific order -
     * otherwise the indexing won't work properly.
     *
     * @param rcpe the entry to add
     * @throws InternalError if entry is {@code null}
     */
    void addEntry(RuntimeCPEntry rcpe) {
        if (rcpe != null) {
            cpEntries.add(rcpe);
        } else {
            throw new InternalError("Constant pool entry while adding was null.");
        }
    }

    /**
     * Get the entry of a specific index. Note: The indexing of the constant pool is from {@code 1} to {@code size()}.
     * There is no element at index 0.
     *
     * @param index index of the entry to return
     * @return the entry at the specified index
     * @throws InternalError if there's no entry at that position
     */
   public RuntimeCPEntry getEntry(int index) {
        if (index > 0 && index <= cpEntries.size()) {
            return cpEntries.get(index - 1);
        } else {
            throw new InternalError("Constant pool doesn't have an item at index "+ index + ".");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(RuntimeCPEntry cpEntry : cpEntries) {
            builder.append(cpEntry.toString()).append("\n");
        }
        return builder.toString();
    }
}
