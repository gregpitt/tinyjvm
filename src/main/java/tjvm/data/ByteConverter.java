package tjvm.data;

import java.nio.ByteBuffer;

/**
 * Util class to convert {@code byte[]} arrays into {@code int} values and the other way around.
 */
public class ByteConverter {

    /**
     * Convert an {@code byte[]} array into an {@code int} value. The {@code byte} array
     * is read in big endian order.
     *
     * @param array the array to convert
     * @return the converted value
     * @throws InternalError if {@code byte} array has more than 4 values
     */
    public static int toSignedInt(byte[] array) {
        if (array.length == 1) {
            return array[0];
        } else if (array.length == 2) {
            return ByteBuffer.wrap(array).getShort();
        } else if (array.length == 4) {
            return ByteBuffer.wrap(array).getInt();
        } else {
            throw new InternalError("Byte array with size " + array.length + " not fitting.");
        }
    }

    /**
     * Convert an {@code byte[]} array into an {@code int} value. The {@code byte} array
     * is read in big endian order.
     *
     * @param array the array to convert
     * @return the converted value
     * @throws InternalError if {@code byte} array has more than 4 values
     */
    public static int toUnsignedInt(byte[] array) {
        if (array.length > 4)
            throw new InternalError("Byte array too long.");
        int result = array[0] & 0xFF;
        for (int i = 1; i < array.length; i++) {
            result = (result << 8) | (array[i] & 0xFF);
        }
        return result;
    }


    /**
     * Convert an {@code int} value into an {@code byte[]} array. The {@code byte[]} array
     * is saved in big endian order.
     * The {@code int} value can be interpreted as:
     * <ul>
     * <li>{@code byte}, if {@code length = 1}</li>
     * <li>{@code short}, if {@code length = 2}</li>
     * <li>{@code int}, if {@code length = 4}</li>
     * </ul>
     *
     * @param length the supposed length of the {@code byte} array
     * @param value  the value to be converted
     * @return the array saved in big endian order
     */
    public static byte[] toBytes(int length, int value) {
        ByteBuffer buffer = ByteBuffer.allocate(length);
        switch (length) {
            case 4:
                buffer.putInt(value);
                break;
            case 2:
                buffer.putShort((short) value);
                break;
            case 1:
                buffer.put((byte) value);
                break;
            default:
                throw new IllegalArgumentException("Can only convert to int, short or byte.");
        }
        return buffer.array();
    }
}
