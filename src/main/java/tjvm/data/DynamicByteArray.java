package tjvm.data;

/**
 * Wrapper class for a {@code byte[]} array that automatically increases in size,
 * if more elements are added, than the array can hold. There is a pointer, which
 * shows the actual position of the next entry. It can also be interpreted as size of
 * the array.<p>
 * Because there are no generic classes as of Java 10 for primitive types, {@link DynamicByteArray}
 * and {@link DynamicIntArray} are held in two seperate classes, having the exact same methods. <p>
 * The {@link DynamicByteArray} is only used by the {@link MethodArea} to store the
 * code of methods in the {@code Method Table}.
 */
class DynamicByteArray {
    /**
     * Initial size of the array
     */
    private static final int INITIAL_SIZE = 1 << 10;

    /**
     * Maximum increment iof the array
     */
    private static final int MAX_INCREMENT = 1 << 20;

    /**
     * Maximum size of the array. This is the biggest value of {@code int}.
     */
    private static final int MAX_SIZE = Integer.MAX_VALUE;

    /**
     * The array.
     */
    private byte[] array;

    /**
     * The pointer to the next element that is inserted. Also the length of the array.
     */
    private int pointer;

    /**
     * Create a new {@code byte[]} array with the the size of {@link DynamicByteArray#INITIAL_SIZE} that
     * is automatically increased in size, if necessary. The pointer to the first entry is at position {@code 0}.
     */
    DynamicByteArray() {
        array = new byte[INITIAL_SIZE];
        pointer = 0;
    }

    /**
     * Add multiple {@code byte[]} elements at the end of the array.
     *
     * @param elements the elements to add
     * @return offset(/ position) of the beginning of the elements in the array
     */
    int addElements(byte[] elements) {
        int begin = pointer;
        while (pointer + elements.length >= array.length)
            increaseSizeOfArray();
        for (byte element : elements) {
            array[pointer] = element;
            pointer++;
        }
        return begin;
    }

    /**
     * Get the position of the next element (can be interpreted as size of the array).
     *
     * @return the position of the next element
     */
    int getPointerPosition() {
        return pointer;
    }

    /**
     * Get one {@code byte} from the array.
     *
     * @param offset the offset(/position) of the element to get in the array
     * @return the element from specified offset
     * @throws ArrayIndexOutOfBoundsException if offset not in array
     */
    byte getElement(int offset) {
        if (offset >= pointer)
            throw (new ArrayIndexOutOfBoundsException());
        return array[offset];
    }

    /**
     * Get multiple {@code byte[]} from the array.
     *
     * @param offset the position of the first element to get in the array
     * @param length the number of elements to get from the first position on
     * @return the elements from specified offset + length
     * @throws ArrayIndexOutOfBoundsException if offset to offset + length not in array
     */
    byte[] getElements(int offset, int length) {
        if (offset + length >= pointer)
            throw new ArrayIndexOutOfBoundsException();
        byte[] res = new byte[length];
        System.arraycopy(array, offset, res, 0, length);
        return res;
    }

    /**
     * Increase the size of the array while keeping the whole content. The pointer stays the same, too.
     */
    private void increaseSizeOfArray() {
        int newSize;
        if (array.length == MAX_SIZE)
            throw (new OutOfMemoryError(""));
        else if (array.length + MAX_INCREMENT >= MAX_SIZE)
            newSize = MAX_SIZE;
        else
            newSize = array.length >= MAX_INCREMENT ? array.length + MAX_INCREMENT : array.length * 2;
        byte[] newArray = new byte[newSize];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }

    /**
     * Set elements in the array. If the elements are longer than the actual size,
     * the {@link #pointer} is set to the end of the {@link #array}.
     *
     * @param elements the elements to set
     * @param base     the start of the future position of the entries in the array
     */
    void setElements(byte[] elements, int base) {
        if (base + elements.length >= array.length)
            throw new ArrayIndexOutOfBoundsException();
        System.arraycopy(elements, 0, array, base, elements.length);
    }
}
