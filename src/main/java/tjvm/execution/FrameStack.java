package tjvm.execution;

import java.util.Stack;

/**
 * Class that represents the {@code Java Virtual Machine stack}, which is a per-{@link Thread}
 * data structure, that stores {@link Frame}s for each method invocation.
 * <blockquote>
 * "Each Java Virtual Machine thread has a private Java Virtual Machine stack, created
 * at the same time as the thread. A Java Virtual Machine stack stores frames (&sect;2.6).
 * A Java Virtual Machine stack is analogous to the stack of a conventional language
 * such as C: it holds local variables and partial results, and plays a part in method
 * invocation and return. Because the Java Virtual Machine stack is never manipulated
 * directly except to push and pop frames, frames may be heap allocated. The memory
 * for a Java Virtual Machine stack does not need to be contiguous." (&sect;2.5.2 JVMS)
 * </blockquote>
 * In this implementation of the JVM, the frame stack is not heap allocated.
 */
class FrameStack {
    private Stack<Frame> stack;

    /**
     * Create an empty frame stack.
     */
    FrameStack() {
        stack = new Stack<>();
    }

    /**
     * Pushes a frame on top of the stack.
     *
     * @param frame the frame to be pushed onto the stack
     */
    void push(Frame frame) {
        stack.push(frame);
    }

    /**
     * Removes the topmost frame from the stack and returns that object as the value.
     *
     * @return the frame which was on top of the stack
     */
    Frame pop() {
        return stack.pop();
    }

    /**
     * Returns the topmost frame from the stack without removing it.
     *
     * @return the frame which is on top of the stack
     */
    Frame peek() {
        return stack.peek();
    }

    /**
     * Test, if there are frames left on the stack.
     *
     * @return {@code true} if there are no frames left
     */
    boolean isEmpty() {
        return stack.isEmpty();
    }
}
