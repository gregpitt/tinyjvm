package tjvm.execution;

/**
 * Class that represents one thread that is run by the Java Virtual Machine.
 * When the Java Virtual Machine has loaded and linked the main class, it starts
 * one thread to execute the main method. That thread can be called the <u>main thread</u>. <br>
 * As for TinyJava, there are no possibilities to run multiple threads at the same time, because
 * the standard libraries of Java are missing due to the language restrictions.<br>
 * To have a run-time data model which fits to the Java Virtual Machine Specification
 * this class explicitly represents the thread in the specification. While some
 * data areas are shared by the threads of one program (or better: process), some
 * data areas are per thread. These per-thread data areas are created when a thread is
 * created and destroyed when the thread exits. These data areas are:
 * <ul>
 * <li>The program counter register ({@link #pc})</li>
 * <li>The Java Virtual Machine Stack ({@link #frameStack}), consisting of one
 * {@link Frame} for each method invocation</li>
 * </ul>
 * Furthermore, the thread is used to implement to locked execution of the &lt;{@code clinit}&gt; method in
 * {@link TinyJVM#initialize(String)}.
 */

class Thread {
    /**
     * The program counter register, which contains the address of the Bytecode
     * in the {@code Byte Code Table} of the{@link tjvm.data.MethodArea} that is currently
     * being executed.
     * <blockquote>
     * "The Java Virtual Machine can support many threads of execution at once (JLS
     * &sect;17). Each Java Virtual Machine thread has its own pc (program counter) register.
     * At any point, each Java Virtual Machine thread is executing the code of a single
     * method, namely the current method (&sect;2.6) for that thread. If that method is not
     * native, the pc register contains the address of the Java Virtual Machine instruction
     * currently being executed." (&sect;2.5.1 JVMS)
     * </blockquote>
     * As there are no native methods in TinyJava, the register always contains a valid address.
     */
    int pc;

    /**
     * The {@code Java Virtual Machine stack}, which stores {@link Frame}s, that are created
     * for each method invocation.
     */
    FrameStack frameStack;

    /**
     * Create a new thread with the {@link #pc} set at {@code 0} and the {@link #frameStack} empty.
     */
    Thread() {
        pc = 0;
        frameStack = new FrameStack();
    }

    /**
     * Method to get the current frame. That frame is the frame, that is currently being executed
     * for this thread. There is always just one current frame per thread.
     *
     * @return the current frame
     */
    Frame currentFrame() {
        return frameStack.peek();
    }

    /**
     * Method to check, if there are {@link Frame}s left on the {@link #frameStack}.
     *
     * @return {@code true}, if the {@link #frameStack} is not empty.
     */
    boolean framesLeft() {
        return !frameStack.isEmpty();
    }
}
