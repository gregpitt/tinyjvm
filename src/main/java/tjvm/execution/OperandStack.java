package tjvm.execution;

import java.util.Stack;

/**
 * Operand stack of values used by the Java Virtual Machine {@link Instruction}s. The values
 * stored on the operand stack are used to store values for processing within methods, e.g.: <p>
 * {@code localVar = 1 + 2;} where {@code 1} and {@code 2} would be stored on the operand stack
 * before adding them.
 * <blockquote>
 * "Each frame (&sect;2.6) contains a last-in-first-out (LIFO) stack known as its operand
 * stack. The maximum depth of the operand stack of a frame is determined at
 * compile-time and is supplied along with the code for the method associated with
 * the frame (&sect;4.7.3).
 * The operand stack is empty when the frame that contains it is created. The
 * Java Virtual Machine supplies instructions to load constants or values from local
 * variables or fields onto the operand stack. Other Java Virtual Machine instructions
 * take operands from the operand stack, operate on them, and push the result back
 * onto the operand stack. The operand stack is also used to prepare parameters to be
 * passed to methods and to receive method results." (&sect;2.6.2 JVMS)
 * </blockquote>
 * The passing of method parameters is handled differently, because of the possibility
 * to use compiled code instead of the {@link Interpreter}. <p>
 * Basically this class is a wrapper for a {@link Stack} with the component type {@code int}.<p>
 * The values of the operands can be of type int, boolean and reference to objects or arrays.
 */
class OperandStack {

    /**
     * The stack of operands.
     */
    private Stack<Integer> stack;
    /**
     * The maximum stack height, that is already verified. In a different implementation
     * of {@link #stack} this could be used to allocate an array-like structure, but in
     * this case is useless.
     */
    private int maxStack;

    /**
     * Create a new, empty operand stack.
     *
     * @param maxStack maximum height of the stack
     */
    OperandStack(int maxStack) {
        this.maxStack = maxStack;
        stack = new Stack<>();
    }

    /**
     * Removes the topmost operand from the stack and returns the value.
     *
     * @return the frame which was on top of the stack
     */
    int pop() {
        return stack.pop();
    }

    /**
     * Pushes an operand on top of the stack.
     *
     * @param op the operand to be pushed onto the stack
     */
    void push(int op) {
        stack.push(op);
    }
}
