package tjvm.verifier;

import tjvm.data.RuntimeCPEntry;
import tjvm.loader.DerivedClass;
import tjvm.loader.Method;
import tjvm.loader.TextChecker;

import java.util.EmptyStackException;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The data-flow analyzer is a class that implements the algorithms given in the "The Java Virtual Machine
 * Specification (Java SE 10 Edition)" in chapter &sect;4.10.2.2, where the data-flow analyzer is part
 * of the {@link BytecodeVerifier}, which has the task, to enforce, that the {@code Code} arrays
 * in the {@code class} files satisfy the static (&sect;4.9.1) and structural (&sect;4.9.1) constraints.
 */
public class DataFlowAnalyzer {

    /**
     * Instructions of the method that is analyzed.
     */
    private NavigableMap<Integer, VerifierInstruction> instructions;

    /**
     * The class the method is from. Necessary for constant pool entries etc.
     */
    private DerivedClass dc;

    /**
     * The method table entry of the method to be analyzed.
     */
    private Method method;

    /**
     * Flag that shows, if the {@code <init>} method for the object of the method needs to be called.
     */
    private boolean needsInitializing;

    /**
     * Id of the object of the method, if it is the {@code <init>} method to check correct initializing.
     */
    private long objectId;

    /**
     * Initialize the data-flow analyzer. This process is described in the "The Java Virtual Machine
     * Specification (Java SE 10 Edition)" as follows:
     * <blockquote>" For the first instruction of the method,
     * the local variables that represent parameters initially contain values of the types
     * indicated by the method's type descriptor; the operand stack is empty. All other
     * local variables contain an illegal value. For the other instructions, which have not
     * been examined yet, no information is available regarding the operand stack or local
     * variables." (&sect;4.10.2.2)</blockquote>
     * Furthermore, instance methods and instance initialization methods require special handling regarding
     * the initialization of the data-flow analysis. This is described as:
     * <blockquote>"When doing data-flow analysis on instance methods, the verifier initializes local
     * variable 0 to contain an object of the current class, or, for instance initialization
     * methods, local variable 0 contains a special type indicating an uninitialized object."
     * (&sect;4.10.2.4)</blockquote>
     *
     * @param instructions the parsed instructions for the analysis
     * @param dc           the derived information from the {@code class} file
     * @param method       the specific derived information of the method
     * @throws VerifyError if more parameters that local variables
     */
    DataFlowAnalyzer(NavigableMap<Integer, VerifierInstruction> instructions, DerivedClass dc, Method method) {
        this.instructions = instructions;
        this.dc = dc;
        this.method = method;
        needsInitializing = false;

        //initialize first instruction
        VerifierInstruction first = instructions.get(instructions.firstKey());
        first.verifierLocals = new VerifierValue[method.maxLocals];
        first.verifierStack = new Stack<>();

        //check local variable length
        if (method.maxLocals < method.argsSize)
            throw new VerifyError("Method " + method.identifier + " has only " + method.maxLocals +
                    " max_locals, whereas it has " + method.argsSize + " arguments.");

        //set first local variable for instance and instance initialization methods
        int pos = 0;
        if (!method.isStatic) {
            if (method.name.equals("<init>")) {
                first.verifierLocals[pos] = new VerifierValue(VerifierType.UNINIT, dc.className);
                objectId = first.verifierLocals[pos].id;
                needsInitializing = true;
            } else {
                first.verifierLocals[pos] = new VerifierValue(VerifierType.OBJECT, dc.className);
            }
            pos++;
        }

        //set other local variables from method parameters
        for (int i = 0; i < method.parameters.size(); i++) {
            first.verifierLocals[pos] = new VerifierValue(method.parameters.get(i), method.parameterNames.get(i));
            pos++;
        }

        //set rest with illegal value
        while (pos < method.maxLocals) {
            first.verifierLocals[pos] = new VerifierValue(VerifierType.ILLEGAL, null);
            pos++;
        }

        //set changed bit for the first instruction
        first.changed = true;

        //set instructions as initialized
        for (VerifierInstruction inst : instructions.values()) {
            inst.verifyStatus = VerifyStatus.INITIALIZED;
        }
    }

    /**
     * Run the {@link DataFlowAnalyzer}, which performs the data-flow analysis. The algorithm is described
     * in the "The Java Virtual Machine Specification (Java SE 10 Edition)" in chapter &sect;4.10.2.2 as follows:
     * <ol>
     * <li>Select a Java Virtual Machine instruction whose "changed" bit is set. If no
     * instruction remains whose "changed" bit is set, the method has successfully
     * been verified. Otherwise, turn off the "changed" bit of the selected instruction.</li>
     * <li>Model the effect of the instruction on the operand stack and local variable array
     * (implemented in {@link #modelEffect(VerifierInstruction)}).</li>
     * <li>Determine the instructions that can follow the current instruction
     * (implemented in {@link #determineSuccessors(VerifierInstruction)}).</li>
     * <li>Merge the state of the operand stack and local variable array at the end of the
     * execution of the current instruction into each of the successor instructions.
     * (implemented  {@link #merge(VerifierInstruction, VerifierInstruction)}).</li>
     * <li>Continue at step 1.</li>
     * </ol>
     */
    public void run() {
        boolean changed;
        do {
            changed = false;
            //1. Select instruction with "changed" bit and turn off
            for (VerifierInstruction inst : instructions.values()) {
                if (inst.changed) {
                    changed = true;
                    inst.changed = false;

                    //2. Model effect of instruction
                    modelEffect(inst);

                    //3. Determine instruction that follows
                    VerifierInstruction[] successors = determineSuccessors(inst);

                    //4. Merge operand stack and local variable array
                    for (VerifierInstruction succ : successors) {
                        merge(inst, succ);
                    }
                    break;
                }
            }
        } while (changed); //5. Continue at step 1
    }

    /**
     * Model the effect of the instruction on the operand stack and local variable array
     * by doing the following:
     * <ul>
     * <li>If the instruction uses values from the operand stack, ensure that there are a
     * sufficient number of values on the stack and that the top values on the stack
     * are of an appropriate type. Otherwise, verification fails.</li>
     * <li>If the instruction uses a local variable, ensure that the specified local variable
     * contains a value of the appropriate type. Otherwise, verification fails.</li>
     * <li>If the instruction pushes values onto the operand stack, ensure that there is
     * sufficient room on the operand stack for the new values. Add the indicated
     * types to the top of the modeled operand stack.</li>
     * <li>If the instruction modifies a local variable, record that the local variable now
     * contains the new type.</li>
     * </ul>
     * Furthermore, Instance Initialization Methods and Newly Created Objects (&sect;4.10.2.4)
     * require a special handling. They are a multistep process and the following rules apply:
     * <ul>
     * <li>A special type indicating an uninitialized object is created and pushed on the
     * verifier's model of the operand stack as the result of the Java Virtual Machine
     * instruction new. The special type indicates the instruction by which the class
     * instance was created and the type of the uninitialized class instance created. When an
     * instance initialization method declared in the class of the uninitialized class instance is
     * invoked on that class instance, all occurrences of the special type are replaced by the intended type of
     * the class instance. This change in type may propagate to subsequent instructions
     * as the dataflow analysis proceeds. <br>
     * After an appropriate instance initialization method is invoked (from the current
     * class or its direct superclass) on this object, all occurrences of this special type
     * on the verifier's model of the operand stack and in the local variable array are
     * replaced by the current class type. The verifier rejects code that uses the new
     * object before it has been initialized or that initializes the object more than once. In
     * addition, it ensures that every normal return of the method has invoked an instance
     * initialization method either in the class of this method or in the direct superclass.
     * </li>
     * <li>The instruction number needs to be stored as part of the special type, as there
     * may be multiple not-yet-initialized instances of a class in existence on the operand
     * stack at one time. For example, the Java Virtual Machine instruction sequence that
     * implements:
     * <blockquote><code>new InputStream(new Foo(), new InputStream("foo"))</code></blockquote>
     * may have two uninitialized instances of InputStream on the operand stack at once.
     * When an instance initialization method is invoked on a class instance, only those
     * occurrences of the special type on the operand stack or in the local variable array
     * that are the same object as the class instance are replaced.
     * </li>
     * </ul>
     *
     * @param inst the current instruction to model the effect for
     * @throws VerifyError if there are incompatible types or other rules violated
     */
    private void modelEffect(VerifierInstruction inst) {
        //1. create deep copies of stack and local variables
        VerifierValue[] locals = new VerifierValue[method.maxLocals];
        for (int i = 0; i < inst.verifierLocals.length; i++) {
            locals[i] = inst.verifierLocals[i].copy();
        }
        Stack<VerifierValue> stack = new Stack<>();
        for (VerifierValue val : inst.verifierStack) {
            stack.add(val.copy());
        }

        VerifierValue val1;
        VerifierValue val2;
        VerifierValue val3;
        VerifierValue val4;
        int localVar;
        String className;
        String descriptor;
        String returnString;
        RuntimeCPEntry cpEntry;
        try {
            switch (inst.type) {
                // jump-instructions
                case IFEQ:
                case IFNE:
                case IFGT:
                case IFGE:
                case IFLT:
                case IFLE:
                    if (stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not an int-value on stack.");
                    break;
                case IF_ICMPEQ:
                case IF_ICMPNE:
                case IF_ICMPGT:
                case IF_ICMPGE:
                case IF_ICMPLT:
                case IF_ICMPLE:
                    if (stack.pop().type != VerifierType.INT || stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not two int-values on stack.");
                    break;

                case IF_ACMPEQ:
                case IF_ACMPNE:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    if (!val1.type.isReference() || !val2.type.isReference())
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not two reference-values on stack.");
                    break;

                case IFNONNULL:
                case IFNULL:
                    if (!stack.pop().type.isReference())
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has a non-reference value on stack.");
                    break;

                case GOTO:
                case GOTO_W:
                    break;

                //local variable instruction
                case ALOAD_0:
                case ALOAD_1:
                case ALOAD_2:
                case ALOAD_3:
                case ALOAD:
                    localVar = inst.type == InstructionType.ALOAD ? inst.op1 : inst.type.opcode - InstructionType.ALOAD_0.opcode;
                    val1 = locals[localVar];
                    if (!val1.type.isReference())
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " accesses local variable at index " + localVar +
                                " which is not a reference.");
                    stack.push(val1);
                    break;

                case ILOAD_0:
                case ILOAD_1:
                case ILOAD_2:
                case ILOAD_3:
                case ILOAD:
                    localVar = inst.type == InstructionType.ILOAD ? inst.op1 : inst.type.opcode - InstructionType.ILOAD_0.opcode;
                    val1 = locals[localVar];
                    if (val1.type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " accesses local variable at index " + localVar +
                                " which is not an int value.");
                    stack.push(val1);
                    break;

                case ASTORE_0:
                case ASTORE_1:
                case ASTORE_2:
                case ASTORE_3:
                case ASTORE:
                    val1 = stack.pop();
                    if (!val1.type.isReference())
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not a reference value on operand stack.");
                    localVar = inst.type == InstructionType.ASTORE ? inst.op1 : inst.type.opcode - InstructionType.ASTORE_0.opcode;
                    locals[localVar] = val1;
                    break;

                case ISTORE_0:
                case ISTORE_1:
                case ISTORE_2:
                case ISTORE_3:
                case ISTORE:
                    val1 = stack.pop();
                    if (val1.type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not an int value on operand stack");
                    localVar = inst.type == InstructionType.ISTORE ? inst.op1 : inst.type.opcode - InstructionType.ISTORE_0.opcode;
                    locals[localVar] = val1;
                    break;

                // constant-pool instructions
                case LDC:
                case LDC_W:
                    stack.push(new VerifierValue(VerifierType.INT, null)); //can not be any other constant pool entry
                    break;

                case NEW:
                    className = dc.runtimeCP.getEntry(inst.op1).identifier;
                    stack.push(new VerifierValue(VerifierType.UNINIT, className));
                    break;

                case PUTFIELD:
                    cpEntry = dc.runtimeCP.getEntry(inst.op1);
                    val1 = stack.pop();
                    descriptor = cpEntry.getDescriptorSubstring();
                    val3 = getVerifierValues(descriptor).get(0);
                    if (!val1.sameType(val3))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible value " + val1.toString() +
                                "compared to required value from descriptor: " + descriptor);
                    val2 = stack.pop();
                    className = cpEntry.getClassSubstring();
                    val4 = new VerifierValue(VerifierType.OBJECT, className);
                    if (!val2.sameType(val4))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible class " + val2.toString() +
                                "compared to required value from className: " + className);
                    break;
                case PUTSTATIC:
                    val1 = stack.pop();
                    descriptor = dc.runtimeCP.getEntry(inst.op1).getDescriptorSubstring();
                    val3 = getVerifierValues(descriptor).get(0);
                    if (!val1.sameType(val3))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible value " + val1.toString() +
                                "compared to required value from descriptor: " + descriptor);
                    break;

                case GETFIELD:
                    val1 = stack.pop();
                    className = dc.runtimeCP.getEntry(inst.op1).getClassSubstring();
                    val2 = new VerifierValue(VerifierType.OBJECT, className);
                    if (!val1.sameType(val2))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible class " + val1.toString() +
                                "compared to required value from className: " + className);
                case GETSTATIC:
                    descriptor = dc.runtimeCP.getEntry(inst.op1).getDescriptorSubstring();
                    val3 = getVerifierValues(descriptor).get(0);
                    stack.push(val3);
                    break;

                case INVOKESTATIC:
                    cpEntry = dc.runtimeCP.getEntry(inst.op1);
                    descriptor = cpEntry.getDescriptorSubstring();
                    checkMethodParameters(descriptor, inst, stack);
                    returnString = descriptor.substring(descriptor.indexOf(")") + 1);
                    if (returnString.equals("I"))
                        stack.push(new VerifierValue(VerifierType.INT, null));
                    break;

                case INVOKESPECIAL:
                    cpEntry = dc.runtimeCP.getEntry(inst.op1);
                    className = cpEntry.getClassSubstring();
                    val1 = stack.pop();
                    if (val1.type != VerifierType.UNINIT ||
                            (!val1.objType.equals(className) && !className.equals("java/lang/Object")))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible objectref on stack "
                                + val1.toString() + " while pointing to constant pool entry " + cpEntry.identifier);
                    for (int i = 0; i < stack.size(); i++)
                        if (stack.get(i).sameType(val1) && stack.get(i).id == val1.id)
                            stack.set(i, new VerifierValue(VerifierType.OBJECT, val1.objType));
                    for (int i = 0; i < locals.length; i++)
                        if (locals[i].sameType(val1) && locals[i].id == val1.id)
                            locals[i] = new VerifierValue(VerifierType.OBJECT, val1.objType);
                    if (val1.id == objectId)
                        needsInitializing = false;
                    break;

                case INVOKEVIRTUAL:
                    cpEntry = dc.runtimeCP.getEntry(inst.op1);
                    descriptor = cpEntry.getDescriptorSubstring();
                    checkMethodParameters(descriptor, inst, stack);
                    val1 = stack.pop();
                    val2 = new VerifierValue(VerifierType.OBJECT, cpEntry.getClassSubstring());
                    if (!val1.sameType(val2))
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has incompatible method arguments " +
                                val1.toString() + " and " + val2.toString());
                    returnString = descriptor.substring(descriptor.indexOf(")") + 1);
                    if (returnString.equals("I")) {
                        stack.push(new VerifierValue(VerifierType.INT, null));
                    }
                    break;

                //various arguments instructions
                case NEWARRAY:
                    if (stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not an int-value on stack.");
                    stack.push(new VerifierValue(VerifierType.INTARR, null));
                    break;
                case BIPUSH:
                case SIPUSH:
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case IINC:
                    if (locals[inst.op1].type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " accesses local variable at index " + inst.op1 +
                                " which is not an int value.");
                    break;

                // no argument instructions
                case NOP:
                    break;
                case POP:
                    stack.pop();
                    break;
                case POP2:
                    stack.pop();
                    stack.pop();
                    break;
                case SWAP:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val1);
                    stack.push(val2);
                    break;
                case DUP:
                    val1 = stack.pop();
                    stack.push(val1.copy());
                    stack.push(val1);
                    break;
                case DUP_X1:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val1.copy());
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP_X2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    stack.push(val1.copy());
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val2.copy());
                    stack.push(val1.copy());
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2_X1:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    stack.push(val2.copy());
                    stack.push(val1.copy());
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2_X2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    val4 = stack.pop();
                    stack.push(val2.copy());
                    stack.push(val1.copy());
                    stack.push(val4);
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case IADD:
                case ISUB:
                case IMUL:
                case IDIV:
                case IAND:
                case IOR:
                case IXOR:
                case IREM:
                case ISHL:
                case ISHR:
                case IUSHR:
                    if (stack.pop().type != VerifierType.INT || stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not two int-values on stack.");
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case INEG:
                    if (stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not an int-value on stack.");
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case ICONST_M1:
                case ICONST_0:
                case ICONST_1:
                case ICONST_2:
                case ICONST_3:
                case ICONST_4:
                case ICONST_5:
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case IALOAD:
                    if (stack.pop().type != VerifierType.INT || stack.pop().type != VerifierType.INTARR)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not index and/or arrayref on stack.");
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case IASTORE:
                    if (stack.pop().type != VerifierType.INT || stack.pop().type != VerifierType.INT ||
                            stack.pop().type != VerifierType.INTARR)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not value, index and/or arrayref on stack.");
                    break;
                case ARRAYLENGTH:
                    if (stack.pop().type != VerifierType.INTARR)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not arrayref on stack.");
                    stack.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case RETURN:
                    if (method.returnsInt)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " should be of type IRETURN.");
                    if (needsInitializing)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " returns before initialization was called.");
                    stack = new Stack<>();
                    break;
                case IRETURN:
                    if (!method.returnsInt)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " should be of type RETURN.");
                    if (needsInitializing)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " returns before initialization was called.");
                    if (stack.pop().type != VerifierType.INT)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not an int-value on stack.");
                    stack = new Stack<>();
                    break;
                case WIDE:
                    throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                            " of method " + method.identifier + " can not be a standalone instruction.");
                default:
                    throw new VerifyError("Instruction at index " + inst.index +
                            " of method " + method.identifier + " is unknown.");
            }

        } catch (EmptyStackException e) {
            throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                    " of method " + method.identifier + " uses empty stack.");
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                    " of method " + method.identifier + " uses local variable greater than max_locals.");
        }

        if (stack.size() > method.maxStack)
            throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                    " of method " + method.identifier + " stack is higher than max_stack.");
        inst.mergeStack = stack;
        inst.mergeLocals = locals;
        inst.verifyStatus = VerifyStatus.VERIFIED;
    }

    /**
     * Determine the instructions that can follow the current instruction. Successor
     * instructions can be one of the following:
     * <ul>
     * <li>The next instruction, if the current instruction is not an unconditional control
     * transfer instruction (for instance, goto, return, or athrow). Verification fails
     * if it is possible to "fall off" the last instruction of the method.</li>
     * <li>The target(s) of a conditional or unconditional branch or switch.</li>
     * <li><s>Any exception handlers for this instruction.</s> (no exception handlers)</li>
     * </ul>
     *
     * @param inst the instruction that is checked for successors
     * @return all possible following instructions, empty array, if there are no successors
     * @throws VerifyError if code can "fall off" the end
     */
    private VerifierInstruction[] determineSuccessors(VerifierInstruction inst) {
        VerifierInstruction[] res;
        switch (inst.type) {
            case IFEQ:
            case IFNE:
            case IFGT:
            case IFGE:
            case IFLT:
            case IFLE:
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPGT:
            case IF_ICMPGE:
            case IF_ICMPLT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
            case IFNONNULL:
            case IFNULL:
                res = new VerifierInstruction[2];
                res[0] = instructions.higherEntry(inst.index).getValue();
                res[1] = instructions.get(inst.index + inst.op1);
                break;
            case GOTO:
            case GOTO_W:
                res = new VerifierInstruction[1];
                res[0] = instructions.get(inst.index + inst.op1);
                break;
            case RETURN:
            case IRETURN:
                res = new VerifierInstruction[0];
                break;
            default:
                res = new VerifierInstruction[1];
                res[0] = instructions.higherEntry(inst.index).getValue();
                break;
        }
        for (VerifierInstruction check : res)
            if (check == null)
                throw new VerifyError("Instruction at index " + inst.index +
                        " of method " + method.identifier + " has invalid successors.");
        return res;
    }

    /**
     * Merge the state of the operand stack and local variable array at the end of the
     * execution of the current instruction into each of the successor instructions, as
     * follows:
     * <ul>
     * <li>If this is the first time the successor instruction has been visited, record that
     * the operand stack and local variable values calculated in step 2 are the state
     * of the operand stack and local variable array prior to executing the successor
     * instruction. Set the "changed" bit for the successor instruction.</li>
     * <li>If the successor instruction has been seen before, merge the operand stack
     * and local variable values calculated in step 2 into the values already there.
     * Set the "changed" bit if there is any modification to the values.</li>
     * </ul>
     * To merge two operand stacks, the number of values on each stack must be identical.
     * Then, corresponding values on the two stacks are compared and the value on the
     * merged stack is computed, as described in {@link #merge(VerifierValue, VerifierValue, int)}.
     * If the operand stacks or local variable arrays cannot be merged, verification of the method fails.
     *
     * @param pred predecessor instruction from which operand stack and local variable array are merged into successor
     * @param succ successor instruction into which operand stack and local variable array are merged
     * @throws VerifyError if predecessor and successor can not be merged by the rules
     */
    private void merge(VerifierInstruction pred, VerifierInstruction succ) {
        //case 1: successor has not been visited before
        if (succ.verifyStatus != VerifyStatus.VERIFIED) {
            succ.verifierLocals = new VerifierValue[method.maxLocals];
            for (int i = 0; i < pred.mergeLocals.length; i++) {
                succ.verifierLocals[i] = pred.mergeLocals[i].copy();
            }
            succ.verifierStack = new Stack<>();
            for (VerifierValue val : pred.mergeStack) {
                succ.verifierStack.add(val.copy());
            }
            succ.changed = true;

        } else { //case 2: successor has been visited before: merge
            //check stack height
            if (pred.mergeStack.size() != succ.verifierStack.size())
                throw new VerifyError("Instruction " + pred.type.name() + " at index " + pred.index +
                        "and successor " + succ.type.name() + " at index " + succ.index +
                        " of method " + method.identifier + " don't have same stack height.");
            //merge stacks
            for (int i = 0; i < pred.mergeStack.size(); i++) {
                VerifierValue val = merge(pred.mergeStack.get(i), succ.verifierStack.get(i), succ.index);
                if (!succ.verifierStack.get(i).sameType(val)) {
                    succ.verifierStack.set(i, val);
                    succ.changed = true;
                }
            }
            //merge local variables
            for (int i = 0; i < pred.mergeLocals.length; i++) {
                VerifierValue val = merge(pred.mergeLocals[i], succ.verifierLocals[i], succ.index);
                if (!succ.verifierLocals[i].sameType(val)) {
                    succ.verifierLocals[i] = val;
                    succ.changed = true;
                }
            }
        }
        succ.verifyStatus = VerifyStatus.VERIFIED;
    }

    /**
     * Parse a descriptor into a stack of verifier values. This can be used for a method or
     * field descriptor. This must be only be consecutive parts of {@code I}, {@code [I} and {@code LClassName;}.
     * Other parts must not be contained in the String, because no checking of the descriptor is done.
     *
     * @param descriptor the descriptor to be parsed
     * @return the stack of values, it is empty, if the descriptor is empty or not valid
     */
    private Stack<VerifierValue> getVerifierValues(String descriptor) {
        //derive parameters from method descriptor
        String paraPattern = TextChecker.INT_DESC + "|" + TextChecker.ARRAY_DESC + "|" + TextChecker.CLASS_DESC;
        Matcher matcher = Pattern.compile(paraPattern).matcher(descriptor);
        Stack<VerifierValue> types = new Stack<>();
        while (matcher.find()) {
            String res = matcher.group();
            switch (res) {
                case "I":
                    types.push(new VerifierValue(VerifierType.INT, null));
                    break;
                case "[I":
                    types.push(new VerifierValue(VerifierType.INTARR, null));
                    break;
                default:
                    types.push(new VerifierValue(VerifierType.OBJECT, res.substring(1, res.length() - 1)));
                    break;
            }
        }
        return types;
    }

    /**
     * Check, if the topmost stack values contain the appropriate parameters for the parameters
     * given in a method descriptor.
     *
     * @param descriptor the {@code String} representation of the method descriptors
     * @param inst       the instruction of the method call, used for better error-description
     * @param stack      the stack to check
     * @throws VerifyError if the stack values do not match the descriptor
     */
    private void checkMethodParameters(String descriptor, VerifierInstruction inst, Stack<VerifierValue> stack) {
        String parameters = descriptor.substring(descriptor.indexOf("(") + 1, descriptor.indexOf(")"));
        Stack<VerifierValue> methodStack = getVerifierValues(parameters);
        while (!methodStack.empty()) {
            try {
                VerifierValue val1 = stack.pop();
                VerifierValue val2 = methodStack.pop();
                if (!val1.sameType(val2))
                    throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                            " of method " + method.identifier + " has incompatible method arguments " +
                            val1.toString() + " and " + val2.toString());
            } catch (EmptyStackException e) {
                throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                        " of method " + method.identifier + " has not enough values on stack.");
            }
        }
    }

    /**
     * Merge two values after the following rules:
     * <ul>
     * <li>If one value is a primitive type, then the corresponding value must be the same
     * primitive type. The merged value is the primitive type.</li>
     * <li>If one value is a non-array reference type, then the corresponding value must
     * be a reference type (array or non-array). The merged value is a reference to
     * an instance of the first common supertype of the two reference types. (Such a
     * reference type always exists because the type Object is a supertype of all class,
     * interface, and array types.)</li>
     * <li>If corresponding values are both array reference types, then their dimensions are
     * examined. If the array types have the same dimensions, then the merged value
     * is a reference to an instance of an array type which is first common supertype
     * of both array types. (If either or both of the array types has a primitive element
     * type, then Object is used as the element type instead.)</li>
     * </ul>
     * To merge two local variable array states, corresponding pairs of local variables
     * are compared. The value of the merged local variable is computed using the rules
     * above, <s>except that the corresponding values are permitted to be different primitive
     * types. In that case, the verifier records that the merged local variable contains an
     * unusable value</s> (only int as primitive).
     *
     * @param pred  the predecessor value
     * @param succ  the successor value
     * @param index the index of the successor for a better error description
     * @return the merged value
     * @throws VerifyError if values can't be merged
     */
    private VerifierValue merge(VerifierValue pred, VerifierValue succ, int index) {
        //case a: one value primitive
        if (pred.type == VerifierType.INT || succ.type == VerifierType.INT) {
            if (pred.type != succ.type)
                throw new VerifyError("Predecessor " + pred.type.name() + "and successor " + succ.type.name()
                        + " at index " + index + " of method " + method.identifier +
                        " can not be merged with int an non-int value.");
            return new VerifierValue(VerifierType.INT, null);

        //case b: one value non-array reference type
        } else if (pred.type == VerifierType.OBJECT || pred.type == VerifierType.UNINIT ||
                succ.type == VerifierType.OBJECT || succ.type == VerifierType.UNINIT) {
            String refType = Objects.equals(pred.objType, succ.objType) ? pred.objType : "java/lang/Object";
            VerifierType type;
            long id = 0;
            if (pred.type == VerifierType.UNINIT || succ.type == VerifierType.UNINIT) {
                type = VerifierType.UNINIT;
                id = pred.type == VerifierType.UNINIT ? pred.id : succ.id; //id of predecessor more relevant
            } else {
                type = VerifierType.OBJECT;
            }
            return new VerifierValue(type, refType, id);

        //case c: values are both array reference types
        } else if (pred.type == VerifierType.INTARR && succ.type == VerifierType.INTARR) {
            return new VerifierValue(VerifierType.INTARR, null);

        } else {
            throw new VerifyError("Instruction " + pred.type.name() + "and successor " + succ.type.name()
                    + " at index " + index + " of method " + method.identifier +
                    " can not be merged.");
        }
    }
}
