package tjvm.verifier;

import tjvm.data.ByteConverter;
import tjvm.data.RuntimeCPEntry;
import tjvm.loader.CPEntry;
import tjvm.loader.DerivedClass;
import tjvm.loader.Method;

import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * The Bytecode Verifier (&sect;4.10.2.2) has the task, to enforce, that the {@code Code} arrays
 * in the {@code class} files satisfy the static (&sect;4.9.1) and structural (&sect;4.9.1) constraints.
 * Additionally there are three rules (see &sect;4.10) that must be enforced on top of that.
 * Usually there are two strategies that implementations of the Java Virtual Machine may use for verification:
 * <ul>
 * <li>The verification by type checking(&sect;4.10.1) is supposed to be used for {@code class} files
 * that are version 50.0 or above. It is based on using a type checker, that applies rules in combination
 * with the {@code StackMapTable}, which is an attribute of the {@code Code} attribute.</li>
 * <li>The verification by type inference(&sect;4.10.2) is supposed to be used for {@code class} files
 * that are below version 50.0. It is based on using an algorithm which contains a data-flow analysis.</li>
 * </ul>
 * Verification by type inference is described as follows:
 * <blockquote>
 * "During linking, the verifier checks the code array of the Code attribute for each
 * method of the class file by performing data-flow analysis on each method. The
 * verifier ensures that at any given point in the program, no matter what code path is
 * taken to reach that point, all of the following are true:
 * <ul>
 * <li>The operand stack is always the same size and contains the same types of values.</li>
 * <li>No local variable is accessed unless it is known to contain a value of an appropriate type. </li>
 * <li>Methods are invoked with the appropriate arguments.</li>
 * <li>Fields are assigned only using values of appropriate types.</li>
 * <li>All opcodes have appropriately typed arguments on the operand stack and in the local variable array.</li>
 * </ul>
 * For efficiency reasons, certain tests that could in principle be performed by the
 * verifier are delayed until the first time the code for the method is actually invoked.
 * In so doing, the verifier avoids loading class files unless it has to.<br>
 * For example, if a method invokes another method that returns an instance of class A, and
 * that instance is assigned only to a field of the same type, the verifier does not bother to
 * check if the class A actually exists. However, if it is assigned to a field of the type B, the
 * definitions of both A and B must be loaded in to ensure that A is a subclass of B." (&sect;4.10.2.1)
 * </blockquote>
 * This implementation of the Bytecode Verifier follows the algorithm given in "The Java Virtual Machine Specification"
 * in chapter &sect;4.10.2.2. The implementation of verification by type checking would be too big for this project.
 * Furthermore, there is no need to load additional classes while verifying, because there are no subclasses
 * in TinyJava.
 */
public class BytecodeVerifier {

    /**
     * Verification of all methods of a class. The algorithm to verify one method can be shortly described as follows:
     * <ol>
     * <li>BREAK up the bytes that the code is made of into instructions and save the index of their starts
     * in the {@code code} array</li>
     * <li>PARSE the instructions, BUILD a data structure and CHECK operands for:
     * <ul>
     * <li>Branches must be within the bounds of the {@code code} array for the method.</li>
     * <li>The targets of all control-flow instructions must be the start of an instruction.</li>
     * <li>No instruction can access or modify a local variable at an index greater than or
     * equal to the number of local variables that its method indicates it allocates.</li>
     * <li>All references to the constant pool must be to an entry of the appropriate type.</li>
     * <li>The code does not end in the middle of an instruction.</li>
     * <li>Execution cannot fall off the end of the code.</li>
     * </ul>
     * </li>
     * <li>INITIALIZE the {@link DataFlowAnalyzer} with the initializer
     * {@link DataFlowAnalyzer#DataFlowAnalyzer(NavigableMap, DerivedClass, Method)}</li>
     * <li>RUN the {@link DataFlowAnalyzer} with the method {@link DataFlowAnalyzer#run()}
     * </ol>
     *
     * @param dc the class representation to verify
     * @throws VerifyError if code does not satify static or structural constraints
     */
    public void verify(DerivedClass dc) {
        for (int i = 0; i < dc.methodList.size(); i++) {
            Method method = dc.methodList.getEntry(i);

            //1. BREAK up the bytes (Navigable Map could be in class Method, but this way shows algorithm better)
            NavigableMap<Integer, VerifierInstruction> instructions;
            instructions = breakUp(method);

            //2. PARSE instructions, BUILD data structure and CHECK operands
            parseBuildAndCheck(instructions, dc, method);

            //3. INITIALIZE data-flow analyzer
            DataFlowAnalyzer analyzer = new DataFlowAnalyzer(instructions, dc, method);

            //4. RUN data-flow analyzer
            analyzer.run();
        }
    }

    /**
     * "BREAK up the bytes that the code is made of into instructions and save the index of their starts
     * in the {@code code} array."
     *
     * @param method the method containing the code to break up
     * @return the instructions
     * @throws VerifyError if instruction opcode is unknown or code is malformed
     */
    public NavigableMap<Integer, VerifierInstruction> breakUp(Method method) {
        byte[] code = method.code;
        NavigableMap<Integer, VerifierInstruction> instructions = new TreeMap<>();
        int i = 0;
        while (i < code.length) {
            //get index
            int index = i;
            boolean isWide = false;

            //get type
            InstructionType type;
            try {
                type = InstructionType.getType(code[i]);
                i++;
            } catch (InternalError e) {
                throw new VerifyError("VerifierInstruction opcode " + code[i] + " at index " + i + " of method "
                        + method.name + " unknown or code malformed.");
            }
            //get next type, if WIDE
            if (type == InstructionType.WIDE) {
                isWide = true;
                try {
                    type = InstructionType.getType(code[i]);
                    i++;
                } catch (InternalError e) {
                    throw new VerifyError("VerifierInstruction opcode " + code[i] + " at index " + i + " of method "
                            + method.name + " after WIDE instruction is unknown or the code is malformed.");
                }
            }

            //get operands, if any
            byte[] operands = new byte[0];
            int opCount = type.byteCount * (isWide ? 2 : 1);
            if (opCount > 0) {
                operands = new byte[opCount];
                System.arraycopy(code, i, operands, 0, opCount);
                i += opCount;
            }
            VerifierInstruction inst = new VerifierInstruction(index, type, operands, isWide);
            inst.verifyStatus = VerifyStatus.DIVIDED;
            instructions.put(index, inst);
        }
        return instructions;
    }

    /**
     * "PARSE the instructions, BUILD a data structure and CHECK operands for:
     * <ul>
     * <li>Branches must be within the bounds of the {@code code} array for the method.</li>
     * <li>The targets of all control-flow instructions must be the start of an instruction.</li>
     * <li>No instruction can access or modify a local variable at an index greater than or
     * equal to the number of local variables that its method indicates it allocates.</li>
     * <li>All references to the constant pool must be to an entry of the appropriate type.</li>
     * <li>The code does not end in the middle of an instruction.</li>
     * <li>Execution cannot fall off the end of the code."</li>
     * </ul>
     * While it has already been checked if the code doesn't end in the middle of an instruction,
     * the rest of the limitations are checked in this method. <br>
     * For the parsing of the operands the method {@link VerifierInstruction#parseOperands()} could be
     * used, but implementing it here too, makes the process more transparent to look at.
     *
     * @param instructions the code of the method broken up into non-parsed instructions
     * @param dc           the class the method is from
     * @param method       the {@link Method} of the method
     * @throws VerifyError if code does not satify static or structural constraints
     */
    void parseBuildAndCheck(NavigableMap<Integer, VerifierInstruction> instructions, DerivedClass dc, Method method) {
        for (VerifierInstruction inst : instructions.values()) {
            RuntimeCPEntry entry;
            String methName;
            switch (inst.type) {
                // jump-instructions
                case IFEQ:
                case IFNE:
                case IFGT:
                case IFGE:
                case IFLT:
                case IFLE:

                case IF_ICMPEQ:
                case IF_ICMPNE:
                case IF_ICMPGT:
                case IF_ICMPGE:
                case IF_ICMPLT:
                case IF_ICMPLE:

                case IF_ACMPEQ:
                case IF_ACMPNE:

                case IFNONNULL:
                case IFNULL:

                case GOTO:
                case GOTO_W:
                    inst.op1 = ByteConverter.toSignedInt(inst.byteOperands);
                    int targetIndex = inst.op1 + inst.index;
                    if (!instructions.containsKey(targetIndex))
                        throw new VerifyError("Jump instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has no valid target (" + targetIndex + ")");
                    break;

                //local variable instruction
                case ALOAD_0:
                case ASTORE_0:
                case ILOAD_0:
                case ISTORE_0:
                    if (0 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable 0 while max_locals is "
                                + method.maxLocals);
                    break;
                case ALOAD_1:
                case ASTORE_1:
                case ILOAD_1:
                case ISTORE_1:
                    if (1 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable 1 while max_locals is "
                                + method.maxLocals);
                    break;
                case ALOAD_2:
                case ASTORE_2:
                case ILOAD_2:
                case ISTORE_2:
                    if (2 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable 2 while max_locals is "
                                + method.maxLocals);
                    break;
                case ALOAD_3:
                case ASTORE_3:
                case ILOAD_3:
                case ISTORE_3:
                    if (3 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable 3 while max_locals is "
                                + method.maxLocals);
                    break;
                case ILOAD:
                case ISTORE:
                case ALOAD:
                case ASTORE:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    if (inst.op1 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable " + inst.op1 +
                                " while max_locals is " + method.maxLocals);
                    break;

                // constant-pool instructions
                case LDC:
                case LDC_W:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    if (dc.runtimeCP.getEntry(inst.op1).tag != CPEntry.CONSTANT_Integer)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " hast index " + inst.op1 + " in constant pool " +
                                inst.op1 + "which is not of type CONSTANT_Integer.");
                    break;
                case NEW:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    if (dc.runtimeCP.getEntry(inst.op1).tag != CPEntry.CONSTANT_Class)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " hast index " + inst.op1 + " in constant pool " +
                                inst.op1 + "which is not of type CONSTANT_Class.");
                    break;
                case PUTFIELD:
                case GETFIELD:
                case GETSTATIC:
                case PUTSTATIC:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    if (dc.runtimeCP.getEntry(inst.op1).tag != CPEntry.CONSTANT_Fieldref)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " hast index " + inst.op1 + " in constant pool " +
                                inst.op1 + "which is not of type CONSTANT_Fieldref.");
                    break;
                case INVOKESTATIC:
                case INVOKESPECIAL:
                case INVOKEVIRTUAL:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    entry = dc.runtimeCP.getEntry(inst.op1);
                    if (entry.tag != CPEntry.CONSTANT_Methodref)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " hast index " + inst.op1 + " in constant pool " +
                                inst.op1 + "which is not of type CONSTANT_Methodref.");

                    methName = entry.getNameSubstring();
                    if (methName.equals("<clinit>") || (methName.equals("<init>") ^ inst.type == InstructionType.INVOKESPECIAL)) //XOR
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " must not be instance or class initialization method.");
                    break;

                //various arguments instructions
                case NEWARRAY:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    if (inst.op1 != 10)
                        throw new VerifyError("Instruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " has not int as its element type.");
                case BIPUSH:
                case SIPUSH:
                    inst.op1 = ByteConverter.toUnsignedInt(inst.byteOperands);
                    break;
                case IINC:
                    int opSize = inst.isWide ? 2 : 1;
                    byte[] op1Array = new byte[opSize];
                    System.arraycopy(inst.byteOperands, 0, op1Array, 0, opSize);
                    inst.op1 = ByteConverter.toUnsignedInt(op1Array);
                    byte[] op2Array = new byte[opSize];
                    System.arraycopy(inst.byteOperands, opSize, op2Array, 0, opSize);
                    inst.op2 = ByteConverter.toSignedInt(op2Array);

                    if (inst.op1 >= method.maxLocals)
                        throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                                " of method " + method.identifier + " modifies local variable " + inst.op1 +
                                " while max_locals is " + method.maxLocals);
                    break;

                // no argument instructions
                case NOP:
                case POP:
                case POP2:
                case SWAP:
                case DUP:
                case DUP_X1:
                case DUP_X2:
                case DUP2:
                case DUP2_X1:
                case DUP2_X2:

                case IADD:
                case ISUB:
                case IMUL:
                case IDIV:
                case IAND:
                case IOR:
                case IXOR:
                case INEG:
                case IREM:
                case ISHL:
                case ISHR:
                case IUSHR:

                case ICONST_M1:
                case ICONST_0:
                case ICONST_1:
                case ICONST_2:
                case ICONST_3:
                case ICONST_4:
                case ICONST_5:

                case IALOAD:
                case IASTORE:

                case ARRAYLENGTH:
                case RETURN:
                case IRETURN:
                    break;

                case WIDE:
                    throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                            " of method " + method.identifier + " can not be a standalone instruction.");
                default:
                    throw new VerifyError("VerifierInstruction at index " + inst.index +
                            " of method " + method.identifier + " is unknown.");
            }
            if (inst.isWide && !inst.type.isWideModifiable())
                throw new VerifyError("VerifierInstruction " + inst.type.name() + " at index " + inst.index +
                        " of method " + method.identifier + " can not be modified by wide-instruction.");

            inst.verifyStatus = VerifyStatus.PARSED;
        }
        //Check if code doesn't fall off the end
        InstructionType lastType = instructions.get(instructions.lastKey()).type;
        if (lastType != InstructionType.RETURN && lastType != InstructionType.IRETURN)
            throw new VerifyError("No return statement at the end of the code - code \"falls off the end\".");
    }
}
