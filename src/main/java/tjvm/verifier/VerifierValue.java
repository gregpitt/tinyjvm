package tjvm.verifier;

import tjvm.loader.Method;
import tjvm.loader.ValueType;

import java.util.Objects;

/**
 * A value that is processed during the data-flow analysis of the {@link DataFlowAnalyzer}. It is stored
 * in the modeled stack and local variables of the {@link VerifierInstruction} before and after its execution.
 */
public class VerifierValue {

    /**
     * Counter to determine identity of uninitialized objects.
     */
    private static long ID_COUNT = 0;

    /**
     * The type of the value.
     */
    VerifierType type;
    /**
     * The type of the (uninitialized) object, if {@link #type} is {@link VerifierType#OBJECT}
     * or {@link VerifierType#UNINIT}.
     */
    String objType;
    /**
     * Id to determine identity of uninitialized objects.
     */
    long id;

    /**
     * Create a new value by specifying type and objectType
     *
     * @param type    the type of the value
     * @param objType the type of the object, {@code null}, if {@link #type} is not an object
     */
    VerifierValue(VerifierType type, String objType) {
        this.type = type;
        this.objType = objType;
        if (type == VerifierType.UNINIT) {
            id = ID_COUNT;
            ID_COUNT++;
        }
    }

    /**
     * Create a new value while using {@link ValueType}s, which are used in the {@link Method}
     *
     * @param type    the type of the value
     * @param objType the type of the object, {@code null}, if {@link #type} is not an object
     */
    VerifierValue(ValueType type, String objType) {
        switch (
                type) {
            case REF:
                this.type = VerifierType.OBJECT;
                this.objType = objType;
                break;
            case INTARR:
                this.type = VerifierType.INTARR;
                break;
            case INT:
                this.type = VerifierType.INT;
                break;
        }
    }

    /**
     * Create a new value by specifying type and objectType and id for uninitialized
     * objects that need the id for initializing all occurrences.
     *
     * @param type    the type of the value
     * @param objType the type of the object, {@code null}, if {@link #type} is not an object
     * @param id      the id of the object, if {@link #type} is {@link VerifierType#UNINIT}
     */
    VerifierValue(VerifierType type, String objType, long id) {
        this.type = type;
        this.objType = objType;
        this.id = id;
    }

    /**
     * Create a deep copy of the verifier value.
     *
     * @return a copy of the verifier value.
     */
    VerifierValue copy() {
        if (type == VerifierType.UNINIT)
            return new VerifierValue(type, objType, id);
        else
            return new VerifierValue(type, objType);
    }

    /**
     * Return a description of the {@link #type} and {@link #objType}.
     *
     * @return the String representation of the value type
     */
    @Override
    public String toString() {
        return type.name() + (objType == null ? "" : " " + objType);
    }

    /**
     * Check, if two verifier values represent the same type. This is the case,
     * if both, the {@link #type} and {@link #objType} are the same.
     *
     * @param other the other object
     * @return {@code true} if both objects represent the same verifier value
     */
    boolean sameType(VerifierValue other) {
        return (other.type == type && Objects.equals(objType, other.objType));
    }
}
