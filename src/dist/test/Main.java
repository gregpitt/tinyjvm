class Main {

    static int b;

//    public static void main(String[] args) {
    static void tinyMain() {
        checkInitializing();
        test();
        methodTest();
        staticTest();
        getAndSetTest();
        controlTest();
        puenschTest();
//        bigLoop();
    }

    static void checkInitializing() {
        b = 2;
        int x;
        System.print(b);
        System.print(StaticClass.constVal);
        System.print(b);
        System.print(StaticClass.test);
        System.print(b);
    }


    static void test() {
        int[] a = new int[1000];
        a[999] = 2;
        a[0] = 5;
        System.print(a[999]);
        System.print(a[0]);

        int euklid;
        euklid = Euklid.ggt(1230120, 1302200);
        System.print(euklid);
    }

    static void methodTest() {
        MethodClass methods;
        methods = new MethodClass();
        System.print(methods.getFinal());

        int res;
        res = methods.intMethod2Param(5, 10);
        System.print(res);

        methods.higherField = 20000;
        int[] test = new int[methods.higherField * 2];
        System.print(test.length);

        methods.voidMethodNoParam();

        System.print(methods.higherField);
        System.print(methods.normalField);
        methods.mathMethod();
        System.print(methods.higherField);
        System.print(methods.normalField);

        System.print(methods.facultyRecursion(30));
    }

    static void staticTest() {
        StaticClass.initValues();

        int a;
        a = StaticClass.getA();
        System.print(a);
        System.print(StaticClass.a);

        int b;
        b = StaticClass.getB();
        System.print(b);
        System.print(StaticClass.b);

        int aNum;
        aNum = StaticClass.getAA(a);
        System.print(aNum);

        int bNum;
        bNum = StaticClass.getBB(StaticClass.getB());
        System.print(bNum);

        int[] newA;
        int[] newB;
        MethodClass methodClass;
        newA = new int[5];
        newB = new int[3];
        newA[(2 * 2 * 2) - 4] = 3;
        methodClass = new MethodClass();
        methodClass.normalField = 1337;
        StaticClass.setAll(4, 2, newA, newB, methodClass);
        System.print(StaticClass.getAA(StaticClass.getA()));
        System.print(StaticClass.getBB(StaticClass.b));

        System.print(StaticClass.getConstVal());
        System.print(StaticClass.mathWithConstVals());
    }

    static void getAndSetTest() {
        GetAndSetClass getAndSetClass;
        getAndSetClass = new GetAndSetClass();

        int a;
        a = getAndSetClass.getA();
        System.print(a);
        System.print(getAndSetClass.a);

        int b;
        b = getAndSetClass.getB();
        System.print(b);
        System.print(getAndSetClass.b);

        int aNum;
        aNum = getAndSetClass.getAA(a);
        System.print(aNum);

        int bNum;
        bNum = getAndSetClass.getBB(getAndSetClass.getB());
        System.print(bNum);

        int[] newA;
        int[] newB;
        MethodClass methodClass;
        StaticClass staticClass;
        newA = new int[22 * 22 + 3];
        newB = new int[107 - 2 * 10];
        newA[(2 * 10 * 10) + 1] = 10;
        newB[10 / 2] = 11 * 11;
        methodClass = new MethodClass();
        staticClass = new StaticClass();
        methodClass.normalField = 1337;
        getAndSetClass.setAll(200, 4, newA, newB, methodClass, staticClass);

        System.print(getAndSetClass.getA());
        System.print(getAndSetClass.getB());
        System.print(getAndSetClass.getAA(getAndSetClass.a));
        System.print(getAndSetClass.getBB(getAndSetClass.getB()));
        methodClass.array[3] = 25;
        System.print(getAndSetClass.getValueFromMethodsArray(3));
    }

    static void controlTest() {
        ControlFlowClass controlFlow;
        controlFlow = new ControlFlowClass();
        controlFlow.whileMethod();
        controlFlow.val1 = -20;
        controlFlow.ifThenElseMethod();
        controlFlow.wideTest();
    }

    static void puenschTest() {
        FlySpaceShip.fly();
    }

    static void bigLoop() {
        int i;
        i = 0;
        MethodClass methods;
        while (i < 1000000000) {
            methods = new MethodClass();
            if(i%100000 == 0)
                System.print(i);
            i = i + 1;
        }
        System.print(1);
    }
}
