class GetAndSetClass {

    //Alle "VarDecl" von Tiny Java (außer static)
    int a;
    int b;
    int[] aa;
    int[] bb;
    MethodClass methods;
    StaticClass statics;

    GetAndSetClass() {
        a = 10;
        b = 200;
        aa = new int[a * 10];
        bb = new int[b + 5];
        methods = new MethodClass();
        statics = new StaticClass();
    }

    void setAll(int newA, int newB, int[] newAA, int[] newBB, MethodClass newMethods, StaticClass newStatics) {
        a = newA;
        b = newB;
        aa = newAA;
        bb = newBB;
        methods = newMethods;
        statics = newStatics;
    }

    int getA() {
        return a;
    }

    int getB() {
        return b;
    }

    int getAA(int at) {
        return aa[at];
    }

    int getBB(int at) {
        return bb[at];
    }

    int getValueFromMethodsArray(int i) {
        int val;
        val = methods.array[i];
        return val;
    }
}
