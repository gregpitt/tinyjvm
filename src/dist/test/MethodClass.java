class MethodClass {

    final int finalField = 129;
    int normalField;
    int higherField;
    int[] array;

    MethodClass() {
        normalField = 50;
        higherField = 2;
        array = new int[normalField + higherField];
    }

    void voidMethodNoParam() {
        final int constInMethod = 1;
        normalField = normalField + constInMethod;
    }

    int intMethod2Param(int bla, int blubb) {
        if (bla > blubb) {
            return bla;
        } else {
            return blubb + finalField;
        }
    }

    int getFinal() {
        return finalField;
    }

    void mathMethod() {
        higherField = (higherField + normalField) - (higherField / normalField);
        normalField = normalField / 4;
    }

    int facultyRecursion(int x) {
        if (x == 0) {
            return 1;
        } else {
            return facultyRecursion(x - 1) * x;
        }
    }

}
