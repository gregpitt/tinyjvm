class StaticClass {

    static StaticClass preInit;
    int initVal;
    final static int test = 2 + mathWithConstVals();
    final static int constVal = 22 - 1232;
    final static int bla = 222323211;
    static int a;
    static int b;
    static int[] aa;
    static int[] bb;
    static MethodClass methods;

    static void initValues() {
        a = 10;
        b = 200;
        aa = new int[a + 50];
        aa[a] = a * 2;
        bb = new int[b * 3];
        bb[b] = b / 4;
        methods = new MethodClass();
    }

    static void setAll(int newA, int newB, int[] newAA, int[] newBB, MethodClass methodClass) {
        a = newA;
        System.print(a);
        b = newB;
        System.print(b);
        aa = newAA;
        System.print(aa[a]);
        bb = newBB;
        System.print(bb[b]);
        methods = methodClass;
        System.print(methodClass.normalField);
    }

    StaticClass() {
        initVal = 1 + test;
    }

    static int mathWithConstVals() {
        preInit = new StaticClass();
        Main.b = 3 + preInit.initVal;
        int ret;
        ret = constVal + ControlFlowClass.calcVal - 1000;
        return ret;
    }

    static int getConstVal() {
        return constVal;
    }

    static int getA() {
        return a;
    }

    static int getB() {
        return b;
    }

    static int getAA(int at) {
        return aa[at];
    }

    static int getBB(int at) {
        return bb[at];
    }

    static int getTest() {
        return test;
    }
}
